[CEffBanX](https://bitbucket.org/mucaho/ceffbanx) ![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/mucaho/ceffbanx/master)
==========

Demo application that showcases multiple FP libraries and wades through the resulting effect zoo.

Motivation
----------

This demo app aims to provide a non-exhaustive overview of available FP libraries for Scala 2.13,
shows possible ways to combine different effect stacks facilitated by those libraries,
and contains _some_ solutions to obstacles encountered along the way.

Libraries
---------

FP-related libraries and their usage in this project:   
* [tagless-final](https://www.baeldung.com/scala/tagless-final-pattern):   
  * notational convention to enforce and use (most often effectful) capabilities that the effect (stack) must facilitate   
* [cats](https://typelevel.org/cats/):   
  * abstractions over pure-effectful computations   
* [cats-effect](https://typelevel.org/cats-effect/):   
  * abstractions over side-effectful computations   
* [cats-mtl](https://typelevel.org/cats-mtl/):   
  * abstractions over effect stacks of combined effectful computations    
* [eff](https://atnos-org.github.io/eff/):   
  * more modular abstractions over effect stacks of combined effectful computations   
  * representation of side-effectful computations as proper data types   
* [scala effekt](https://b-studios.de/scala-effekt/)   
  * abstractions over algebraic effects   
* [izumi bio](https://izumi.7mind.io/bio/index.html):   
  * abstractions over abstractions of effectful computations   
* [fs2](https://fs2.io):   
  * abstractions over effectful computation streams   
* [tofu](https://docs.tofu.tf/):   
  * abstractions over abstractions of effectful computation streams   
  * higher-kinded abstractions over effectful computation streams   
  * abstractions over effectful logging   

Notable FP library that is not covered:   
* [monix](https://monix.io/)   
  * abstractions over side-effectful computations   
* [monix-bio](https://bio.monix.io/)   
  * bi-functor abstractions over side-effectful computations   
* [zio](https://zio.dev/)   
  * tri-functor abstractions over side-effectful computations   

Other tools used:   
* [cats stm](https://timwspence.github.io/cats-stm/):   
  * effectful software-transactional memory   
* [refined](https://github.com/fthomas/refined):   
  * type-level refined (primitive) data types   
* [monix-newtype](https://newtypes.monix.io/)   
  * type-level-distinct data types of the same underlying type   
* [kittens](https://github.com/typelevel/kittens)   
  * semi-automatic type class derivations   
* [weaver](https://disneystreaming.github.io/weaver-test/)   
  * test runner for effectful computation (streams)   
* [weaver-test-extra](https://github.com/dimitarg/weaver-test-extra)   
  * test definitions as effectful computations   
* [expecty](https://github.com/eed3si9n/expecty/)   
  * test assertions   
* [scalacheck](https://scalacheck.org/)   
  * property-based test assertions   
* [discipline](https://github.com/typelevel/discipline)   
  * test assertions for algebra laws   
* [natchez](https://github.com/typelevel/natchez)   
  * tracing for effectful computations   
* [log4cats](https://typelevel.org/log4cats/)   
  * logging for effectful computations   
* [kind-projector](https://github.com/typelevel/kind-projector):   
  * compiler plugin for easier type projections   
* [better-monadic-for](https://github.com/oleg-py/better-monadic-for):   
  * compiler plugin for enhanced for-comprehensions   
* [sbt-tpolecat](https://github.com/typelevel/sbt-tpolecat)   
  * compiler plugin for enforcing stricter compilation checks   
* [wartremover](https://www.wartremover.org/)   
  * compiler plugin for static code analysis   
* [scalafix](https://scalacenter.github.io/scalafix/)   
  * compiler plugin for refactoring and linting   
* [scalafmt](https://scalameta.org/scalafmt/)    
  * compiler plugin for code formatting   

Disclaimer
----------

This application should **not** be used as a template for production code,
as it is ill-advised to combine various FP libraries like that.
Some constructs are deliberately made to shoot themselves in the foot
to simulate interop with 3rd party APIs and their read-only method signatures.
Such combination is merely explored here for educational purposes.

Moreover, some effect stacks are not properly lifted to other effect stacks in this app,
but are rather evaluated and their result is lifted into the target stack, instead.
In these cases proper interop between those effect stacks seems non-trivial, at least from my current understanding.

Use case
--------

As this project name hints at, this app runs multiple transactions of funds between customers
in a contrived banking system, in parallel.

Execute `sbt run` to run the example and `sbt test` for the tests.

Contributing
------------

Suggestions and improvements are very welcome!
Please feel free to open appropriate discussion threads, issues and PRs.
