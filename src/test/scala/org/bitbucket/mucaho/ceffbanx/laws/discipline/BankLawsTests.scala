package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import cats.Eq
import cats.data.{State, Writer}
import eu.timepit.refined.scalacheck.all._
import org.atnos.eff.{Eff, Fx}
import org.bitbucket.mucaho.ceffbanx.Util.EffInterpretation.{interpretStateEffect, runEffect}
import org.bitbucket.mucaho.ceffbanx.domain.{Bank, DomainUtil}
import org.bitbucket.mucaho.ceffbanx.domain.Bank._
import org.bitbucket.mucaho.ceffbanx.domain.Common.{BALANCE_ZERO, Balance}
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultBankInterpreter.interpretBankEffects
import weaver.FunSuite
import weaver.discipline.Discipline

object BankLawsTests extends FunSuite with Discipline with DomainUtil {

  { /* DefaultBankInterpreter */
    type BankEffect[A] = Eff[Fx.fx2[BankAlgebra, State[Balance, *]], A]

    implicit def bankEffectEq[A: Eq]: Eq[BankEffect[A]] = Eq.by { (eff: BankEffect[A]) =>
      val (value, balance) = runEffect {
        interpretStateEffect(defaultBalance) {
          interpretBankEffects { eff }
        }
      }

      (value, balance)
    }

    implicit val bank: Bank[BankEffect] = Bank.makeAlgebra

    checkAll("DefaultBankInterpreter", BankRuleSet[BankEffect].ruleSet)
  }

  { /* CommandWriter */
    type BankEffect[A] = Writer[Seq[BankAlgebra[Any]], A]

    implicit def bankEffectEq[A: Eq]: Eq[BankEffect[A]] = Eq.by { (eff: BankEffect[A]) =>
      {
        val (commands, value) = eff.run
        val balance = commands.map {
          case AdaptBalanceBy(amount) => amount.value
          case _                      => BALANCE_ZERO.value
        }.sum

        (balance, value)
      }
    }

    implicit val bank: Bank[BankEffect] = Bank.makeCommandWriter

    checkAll("CommandWriter", BankRuleSet[BankEffect].ruleSet)
  }

  { /* CommandStringWriter */
    type BankEffect[A] = Writer[Seq[String], A]

    implicit def bankEffectEq[A: Eq]: Eq[BankEffect[A]] =
      Eq.by(_.value) // TEST IS UNSOUND!!!

    implicit val bank: Bank[BankEffect] = Bank.makeCommandStringWriter

    checkAll("CommandStringWriter", BankRuleSet[BankEffect].ruleSet)
  }
}
