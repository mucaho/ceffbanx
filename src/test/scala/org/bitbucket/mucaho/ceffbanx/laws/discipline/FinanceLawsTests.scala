package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import cats.Eq
import cats.data.{NonEmptySeq, State}
import cats.derived
import eu.timepit.refined.scalacheck.all._
import izumi.functional.bio.Monad2
import org.atnos.eff.{Eff, Fx}
import org.bitbucket.mucaho.ceffbanx.Util.EffInterpretation.{
  interpretEitherEffect,
  interpretStateEffect,
  runEffect
}
import org.bitbucket.mucaho.ceffbanx.domain.Account.AccountAlgebra
import org.bitbucket.mucaho.ceffbanx.domain.Bank.BankAlgebra
import org.bitbucket.mucaho.ceffbanx.domain.Common.{Balance, CustomerId, Standing}
import org.bitbucket.mucaho.ceffbanx.domain.Ledger.LedgerAlgebra
import org.bitbucket.mucaho.ceffbanx.domain.{DomainUtil, Finance}
import org.bitbucket.mucaho.ceffbanx.domain.Finance._
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultAccountInterpreter.transformAccountEffects
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultBankInterpreter.interpretBankEffects
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultFinanceInterpreter.interpretFinancialEffects
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultLedgerInterpreter.interpretLedgerEffects
import weaver.FunSuite
import weaver.discipline.Discipline

import scala.annotation.unchecked.uncheckedVariance

object FinanceLawsTests extends FunSuite with Discipline with DomainUtil {

  { /* DefaultFinanceInterpreter */
    // format: off
    type EffectStack = Fx.fx7[
      FinancialAlgebra[FinancialErrorEvent, *],
      BankAlgebra,
      AccountAlgebra,
      LedgerAlgebra,
      State[Balance, *],
      State[Map[CustomerId, Standing], *],
      Either[Throwable, *]
    ]
    // format: on
    type FinanceEffect[A] = Eff[EffectStack, A]
    type BiFinanceEffect[+E, +A] = Eff[EffectStack, A @uncheckedVariance]

    implicit val eqThrowable: Eq[Throwable] = Eq.by(_.getMessage)

    implicit val eqUpdatedCustomerBalance: Eq[UpdatedCustomerBalance] =
      derived.semiauto.eq
    implicit val eqUpdatedCustomerStandings: Eq[UpdatedCustomerStandings] =
      derived.semiauto.eq
    implicit val eqUpdatedBankBalance: Eq[UpdatedBankBalance] =
      derived.semiauto.eq
    implicit val eqSuccessEvt: Eq[FinancialSuccessEvent] = derived.semiauto.eq

    implicit def biFinanceEffectEq[E, A: Eq]: Eq[BiFinanceEffect[E, A]] =
      Eq.by { (eff: BiFinanceEffect[E, A]) =>
        {
          val higherInterpreted =
            interpretLedgerEffects {
              transformAccountEffects {
                interpretBankEffects {
                  interpretFinancialEffects { eff }
                }
              }
            }

          val lowerInterpreted =
            interpretEitherEffect {
              interpretStateEffect(defaultStandingMap) {
                interpretStateEffect(defaultBalance) {
                  interpretStateEffect(defaultBalanceMap) {
                    higherInterpreted
                  }
                }
              }
            }

          val result = runEffect {
            lowerInterpreted
          }

          result
        }
      }

    implicit val eqErrorSuccessEffect
      : Eq[BiFinanceEffect[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
      biFinanceEffectEq[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]

    implicit val eqErrorSuccessEffect2
      : Eq[BiFinanceEffect[FinancialErrorEvent, Set[FinancialSuccessEvent]]] =
      biFinanceEffectEq[FinancialErrorEvent, Set[FinancialSuccessEvent]]

    implicit val finance: Finance[BiFinanceEffect] =
      FinanceFactory[EffectStack]().makeAlgebra

    implicit val monad: Monad2[BiFinanceEffect] =
      new Util.CatsBIOImplicitInstances.Factory[FinanceEffect].monadInstance

    checkAll(
      "DefaultFinanceInterpreter",
      FinanceRuleSet[BiFinanceEffect]
        .ruleSet(implicitly, implicitly, implicitly, eqErrorSuccessEffect, eqErrorSuccessEffect2)
    )
  }

  // TODO: add tests for other Finance implementations, similar to other algebra tests
}
