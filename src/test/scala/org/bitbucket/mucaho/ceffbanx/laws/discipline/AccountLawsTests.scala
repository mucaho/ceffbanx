package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import cats.Eq
import cats.data.Writer
import eu.timepit.refined.scalacheck.all._
import org.atnos.eff.{Eff, Fx}
import org.bitbucket.mucaho.ceffbanx.Util.EffInterpretation.{interpretStateEffect, runEffect}
import org.bitbucket.mucaho.ceffbanx.domain.{Account, DomainUtil}
import org.bitbucket.mucaho.ceffbanx.domain.Account._
import org.bitbucket.mucaho.ceffbanx.domain.Common.BALANCE_ZERO
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultAccountInterpreter.transformAccountEffects
import weaver.FunSuite
import weaver.discipline.Discipline

object AccountLawsTests extends FunSuite with Discipline with DomainUtil {

  { /* DefaultAccountInterpreter */
    type AccountEffect[A] = Eff[Fx.fx1[AccountAlgebra], A]

    implicit def accountEffectEq[A: Eq]: Eq[AccountEffect[A]] = Eq.by { (eff: AccountEffect[A]) =>
      val (value, map) = runEffect {
        interpretStateEffect(defaultBalanceMap) {
          transformAccountEffects { eff }
        }
      }

      // remove assignments with default value for map equality to work properly throughout tests
      (value, streamlineBalanceMap(map))
    }

    implicit val account: Account[AccountEffect] = Account.makeAlgebra

    checkAll("DefaultAccountInterpreter", AccountRuleSet[AccountEffect].ruleSet)
  }

  { /* CommandWriter */
    type AccountEffect[A] = Writer[Seq[AccountAlgebra[Any]], A]

    implicit def accountEffectEq[A: Eq]: Eq[AccountEffect[A]] = Eq.by { (eff: AccountEffect[A]) =>
      {
        val (commands, value) = eff.run
        val balance = commands.map {
          case AdaptBalanceBy(_, amount) => amount.value
          case _                         => BALANCE_ZERO.value
        }.sum

        (balance, value)
      }
    }

    implicit val account: Account[AccountEffect] = Account.makeCommandWriter

    checkAll("CommandWriter", AccountRuleSet[AccountEffect].ruleSet)
  }

  { /* CommandStringWriter */
    type AccountEffect[A] = Writer[Seq[String], A]

    implicit def accountEffectEq[A: Eq]: Eq[AccountEffect[A]] =
      Eq.by(_.value) // TEST IS UNSOUND!!!

    implicit val account: Account[AccountEffect] =
      Account.makeCommandStringWriter

    checkAll("CommandStringWriter", AccountRuleSet[AccountEffect].ruleSet)
  }
}
