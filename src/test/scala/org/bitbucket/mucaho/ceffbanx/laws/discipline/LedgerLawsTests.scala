package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import cats.Eq
import cats.data.{State, Writer}
import eu.timepit.refined.scalacheck.all._
import org.atnos.eff.{Eff, Fx}
import org.bitbucket.mucaho.ceffbanx.Util.EffInterpretation.{interpretStateEffect, runEffect}
import org.bitbucket.mucaho.ceffbanx.domain.Common.{CustomerId, STANDING_ZERO, Standing}
import org.bitbucket.mucaho.ceffbanx.domain.{DomainUtil, Ledger}
import org.bitbucket.mucaho.ceffbanx.domain.Ledger._
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultLedgerInterpreter.interpretLedgerEffects
import weaver.FunSuite
import weaver.discipline.Discipline

object LedgerLawsTests extends FunSuite with Discipline with DomainUtil {

  { /* DefaultLedgerInterpreter */
    type LedgerEffect[A] =
      Eff[Fx.fx2[LedgerAlgebra, State[Map[CustomerId, Standing], *]], A]
    implicit def ledgerEffectEq[A: Eq]: Eq[LedgerEffect[A]] = Eq.by { (eff: LedgerEffect[A]) =>
      val (value, map) = runEffect {
        interpretStateEffect(defaultStandingMap) {
          interpretLedgerEffects { eff }
        }
      }

      // remove assignments with default value for map equality to work properly throughout tests
      (value, streamlineStandingMap(map))
    }

    implicit val ledger: Ledger[LedgerEffect] = Ledger.makeAlgebra

    checkAll("DefaultLedgerInterpreter", LedgerRuleSet[LedgerEffect].ruleSet)
  }

  { /* CommandWriter */
    type LedgerEffect[A] = Writer[Seq[LedgerAlgebra[Any]], A]

    implicit def ledgerEffectEq[A: Eq]: Eq[LedgerEffect[A]] = Eq.by { (eff: LedgerEffect[A]) =>
      {
        val (commands, value) = eff.run
        val standing = commands.map {
          case AdaptStandings(_, amount) => amount.value
          case _                         => STANDING_ZERO.value
        }.sum

        (standing, value)
      }
    }

    implicit val ledger: Ledger[LedgerEffect] = Ledger.makeCommandWriter

    checkAll("CommandWriter", LedgerRuleSet[LedgerEffect].ruleSet)
  }

  { /* CommandStringWriter */
    type LedgerEffect[A] = Writer[Seq[String], A]

    implicit def ledgerEffectEq[A: Eq]: Eq[LedgerEffect[A]] =
      Eq.by(_.value) // TEST IS UNSOUND!!!

    implicit val ledger: Ledger[LedgerEffect] = Ledger.makeCommandStringWriter

    checkAll("CommandStringWriter", LedgerRuleSet[LedgerEffect].ruleSet)
  }
}
