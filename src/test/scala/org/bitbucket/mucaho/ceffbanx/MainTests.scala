package org.bitbucket.mucaho.ceffbanx

import cats.effect.{IO, Resource}
import fs2.Stream
import io.github.timwspence.cats.stm.STM
import natchez.Span
import org.bitbucket.mucaho.ceffbanx.Main.runWithSTM
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Finance.{
  UpdatedBankBalance,
  UpdatedCustomerBalance,
  UpdatedCustomerStandings
}
import weaver.SimpleIOSuite
import weaver.pure.traced.{tracedParSuite, tracedTest}

object MainTests extends SimpleIOSuite with ContextSuite {

  tracedTestSuite("FEATURE: Main.runWithStm") { implicit span: Span[IO] =>
    {
      import cats.implicits._

      tracedContext("GIVEN a default stm") {
        Resource.eval(STM.runtime[IO])
      } { stm => implicit span =>
        tracedContext("WHEN evaluating the returned result") {
          Resource.eval {
            import tofu.fs2Instances._
            runWithSTM[Stream[IO, *], IO](stm)
              .flatTap(outs => IO(println(outs)))
          }
        } { case (initialState, events, finalState) =>
          implicit span =>
            tracedParSuite("THEN the result inhabits the following characteristics")(
              List(
                tracedTest("initial bank balance is maxed") { _ =>
                  {
                    val (initialBankBalance, _, _) = initialState
                    expect(initialBankBalance._2 == BALANCE_MAX).pure[IO]
                  }
                },
                tracedTest("initial customer balances are zero") { _ =>
                  {
                    val (_, initialCustomerBalances, _) = initialState
                    expect(
                      initialCustomerBalances.values
                        .forall(_._2 == BALANCE_ZERO)
                    ).pure[IO]
                  }
                },
                tracedTest("initial customer standings are zero") { _ =>
                  {
                    val (_, _, initialCustomerStandings) = initialState
                    expect(
                      initialCustomerStandings.values
                        .forall(_._2 == STANDING_ZERO)
                    ).pure[IO]
                  }
                },
                tracedTest("final state is different from initial state") { _ =>
                  expect(finalState != initialState).pure[IO]
                },
                tracedTest("there exist error events") { _ =>
                  expect(events.exists(_.isLeft)).pure[IO]
                },
                tracedTest("there exist success events") { _ =>
                  expect(events.exists(_.isRight)).pure[IO]
                },
                tracedTest(
                  "initial bank balance matches final bank balance plus sum of final customer balances"
                ) { _ =>
                  {
                    val (initialBankBalance, _, _) = initialState
                    val (finalBankBalance, finalCustomerBalances, _) =
                      finalState
                    val finalCustomerBalance = finalCustomerBalances.view.values
                      .map(_._2.value)
                      .fold(BALANCE_ZERO.value)(_ + _)
                    expect(
                      initialBankBalance._2.value == (finalCustomerBalance + finalBankBalance._2.value)
                    ).pure[IO]
                  }
                },
                tracedTest(
                  "final bank balance matches initial bank balance plus sum of final customer standings"
                ) { _ =>
                  {
                    val (initialBankBalance, _, _) = initialState
                    val (finalBankBalance, _, finalCustomerStandings) =
                      finalState
                    val finalCustomerStanding =
                      finalCustomerStandings.view.values
                        .map(_._2.value)
                        .fold(STANDING_ZERO.value)(_ + _)
                    expect(
                      finalBankBalance._2.value == (initialBankBalance._2.value + finalCustomerStanding)
                    ).pure[IO]
                  }
                },
                tracedTest("latest event for bank balances matches final bank balance") { _ =>
                  {
                    val (finalBankBalance, _, _) = finalState
                    val latestBankBalance = events
                      .findLast {
                        case Right(evts) =>
                          evts.exists {
                            case UpdatedBankBalance(_, _) => true
                            case _                        => false
                          }
                        case _ => false
                      }
                      .flatMap {
                        case Right(evts) =>
                          evts.toSeq
                            .findLast {
                              case UpdatedBankBalance(_, _) => true
                              case _                        => false
                            }
                            .map(_.asInstanceOf[UpdatedBankBalance].amount)
                        case Left(_) => None
                      }

                    (latestBankBalance match {
                      case Some(latestUpdatedBankBalance) =>
                        expect(finalBankBalance == latestUpdatedBankBalance)
                      case None => failure("no bank balance event found")
                    }).pure[IO]
                  }
                },
                tracedTest("latest event for bank balances matches final bank balance") { _ =>
                  {
                    val emptyCustomerBalances = ALL_VALID_CUSTOMERS_LIST
                      .map(_ -> (VERSION_ZERO -> BALANCE_ZERO))
                      .toMap

                    val (_, finalCustomerBalances, _) = finalState
                    val latestCustomerBalances = events
                      .foldLeft(emptyCustomerBalances) { case (map, elemEvts) =>
                        val updatedCustomerBalances = elemEvts match {
                          case Right(evts) =>
                            evts.collect { case UpdatedCustomerBalance(customerId, amount, _) =>
                              customerId -> amount
                            }
                          case _ => Seq.empty
                        }

                        (map.toSeq ++ updatedCustomerBalances).groupMapReduce(_._1)(_._2) {
                          case (prevBal, newBal) =>
                            if (versionOrdering.compare(newBal._1, prevBal._1) > 0) newBal
                            else prevBal
                        }
                      }

                    expect(finalCustomerBalances == latestCustomerBalances)
                      .pure[IO]
                  }
                },
                tracedTest("latest event for customer standing matches final customer standing") {
                  _ =>
                    {
                      val emptyCustomerStandings = ALL_VALID_CUSTOMERS_LIST
                        .map(_ -> (VERSION_ZERO -> STANDING_ZERO))
                        .toMap

                      val (_, _, finalCustomerStandings) = finalState
                      val latestCustomerStandings =
                        events
                          .foldLeft(emptyCustomerStandings) { case (map, elemEvts) =>
                            val updatedCustomerStandings = elemEvts match {
                              case Right(evts) =>
                                evts.collect {
                                  case UpdatedCustomerStandings(customerId, amount, _) =>
                                    customerId -> amount
                                }
                              case _ => Seq.empty
                            }

                            (map.toSeq ++ updatedCustomerStandings)
                              .groupMapReduce(_._1)(_._2) { case (prevStand, newStand) =>
                                if (
                                  versionOrdering
                                    .compare(newStand._1, prevStand._1) > 0
                                ) newStand
                                else prevStand
                              }
                          }

                      expect(finalCustomerStandings == latestCustomerStandings)
                        .pure[IO]
                    }
                }
              )
            )
        }
      }
    }
  }
}
