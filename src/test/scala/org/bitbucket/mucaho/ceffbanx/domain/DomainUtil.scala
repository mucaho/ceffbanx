package org.bitbucket.mucaho.ceffbanx
package domain

import cats.Eq
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.scalacheck.{Arbitrary, Gen}

trait DomainUtil {
  implicit def arbCustomerId: Arbitrary[CustomerId] = Arbitrary {
    // needed as refined's scalacheck generator cannot produce such complex refined values
    Gen.oneOf(ANY_CUSTOMERS_LIST)
  }

  implicit val customerIdEq: Eq[CustomerId] = Eq.fromUniversalEquals

  // for the sake of testing ignore the version for equality check
  implicit val balanceEq: Eq[Balance] = (x: Balance, y: Balance) => x._2.value == y._2.value

  val defaultBalance: Balance = VERSION_ZERO -> BALANCE_MID
  val defaultBalanceMap: Map[CustomerId, Balance] =
    ANY_CUSTOMERS_LIST.map(_ -> (VERSION_ZERO -> BALANCE_MID)).toMap
  def streamlineBalanceMap(map: Map[CustomerId, Balance]): Map[CustomerId, Balance] =
    map.filterNot { case (_, balance) => balance._2 == BALANCE_MID }

  // for the sake of testing ignore the version for equality check
  implicit val standingEq: Eq[Standing] = (x: Standing, y: Standing) => x._2.value == y._2.value

  val defaultStanding: Standing = VERSION_ZERO -> STANDING_ZERO
  val defaultStandingMap: Map[CustomerId, Standing] =
    ANY_CUSTOMERS_LIST.map(_ -> (VERSION_ZERO -> STANDING_ZERO)).toMap
  def streamlineStandingMap(map: Map[CustomerId, Standing]): Map[CustomerId, Standing] =
    map.filterNot { case (_, standing) => standing._2 == STANDING_ZERO }
}
