package org.bitbucket.mucaho.ceffbanx

import cats.data.NonEmptySeq
import cats.effect.{IO, Resource}
import fs2.Stream
import natchez.Span
import natchez.log.Log
import org.bitbucket.mucaho.ceffbanx.MainTests.{success, test}
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import weaver.TestName
import weaver.pure.Test

trait ContextSuite {
  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  def tracedContext[R](name: String)(
    resource: Resource[IO, R]
  )(fn: R => Span[IO] => Stream[IO, Test])(implicit rootSpan: Span[IO]): Stream[IO, Test] = {
    Stream.resource(rootSpan.span(name)).flatMap { childSpan: Span[IO] =>
      Stream.resource(resource).flatMap(res => fn(res)(childSpan))
    }
  }

  def tracedSeqContexts[R](name: String)(resource: Resource[IO, R])(
    fn: R => Span[IO] => NonEmptySeq[Stream[IO, Test]]
  )(implicit rootSpan: Span[IO]): Stream[IO, Test] = {
    Stream.resource(rootSpan.span(name)).flatMap { childSpan: Span[IO] =>
      Stream.resource(resource).flatMap(res => fn(res)(childSpan).reduce)
    }
  }

  def tracedParContexts[R](name: String)(resource: Resource[IO, R])(
    fn: R => Span[IO] => NonEmptySeq[Stream[IO, Test]]
  )(implicit rootSpan: Span[IO]): Stream[IO, Test] = {
    Stream.resource(rootSpan.span(name)).flatMap { childSpan: Span[IO] =>
      Stream
        .resource(resource)
        .flatMap(res => {
          import fs2.Stream._

          val testStreams = fn(res)(childSpan)
          Stream
            .emits(testStreams.toSeq)
            .parJoin(Runtime.getRuntime.availableProcessors * 2)
        })
    }
  }

  def tracedTestSuite(name: TestName)(run: Span[IO] => Stream[IO, Test]): Unit = {
    test(name) {
      Log
        .entryPoint[IO](this.getClass.getCanonicalName)
        .root(name.name)
        .use(span => {
          val testStream = run(span)
          val tests = testStream.compile.toList
          val expectations = tests.map(_.map(_.run))
          val expectation =
            expectations.map(_.fold(success) { case (l, r) => l && r })
          expectation
        })
    }
  }
}
