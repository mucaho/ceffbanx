package org.bitbucket.mucaho.ceffbanx
package domain

import cats.data.Writer
import org.atnos.eff.Interpret.translate
import org.atnos.eff.{Eff, Member, Translate, |=}
import org.bitbucket.mucaho.ceffbanx.domain.Common.{
  CustomerId,
  DeltaAmount,
  STANDING_ZERO,
  Standing,
  VERSION_ZERO
}

trait Ledger[F[_]] {
  def getStandings(customerId: CustomerId): F[Standing]
  def adaptStandings(customerId: CustomerId, amount: DeltaAmount): F[Boolean]
}

object Ledger {
  sealed trait LedgerAlgebra[+A]
  final case class GetStandings(customerId: CustomerId) extends LedgerAlgebra[Standing]
  // can never go below a certain amount; return value indicates success
  final case class AdaptStandings(customerId: CustomerId, amount: DeltaAmount)
      extends LedgerAlgebra[Boolean]

  object LedgerAlgebra {
    def getStandings[R: LedgerAlgebra |= *](customerId: CustomerId): Eff[R, Standing] =
      Eff.send(GetStandings(customerId))
    def adaptStandings[R: LedgerAlgebra |= *](
      customerId: CustomerId,
      amount: DeltaAmount
    ): Eff[R, Boolean] =
      Eff.send(AdaptStandings(customerId, amount))
  }

  def makeAlgebra[R: LedgerAlgebra |= *]: Ledger[Eff[R, *]] =
    new Ledger[Eff[R, *]] {
      override def getStandings(customerId: CustomerId): Eff[R, Standing] =
        LedgerAlgebra.getStandings(customerId)
      override def adaptStandings(customerId: CustomerId, amount: DeltaAmount): Eff[R, Boolean] =
        LedgerAlgebra.adaptStandings(customerId, amount)
    }

  def makeAlgebraWrapper[R, F[_]](
    wrappedInterpreter: Ledger[F]
  )(implicit ev: F |= R): Ledger[Eff[R, *]] =
    new Ledger[Eff[R, *]] {
      override def getStandings(customerId: CustomerId): Eff[R, Standing] =
        Eff.send(wrappedInterpreter.getStandings(customerId))
      override def adaptStandings(customerId: CustomerId, amount: DeltaAmount): Eff[R, Boolean] =
        Eff.send(wrappedInterpreter.adaptStandings(customerId, amount))
    }

  def interpretAlgebraWrapperEffects[R, U, A](
    wrappingInterpreter: Ledger[Eff[U, *]]
  )(effects: Eff[R, A])(implicit ledgerM: Member.Aux[LedgerAlgebra, R, U]): Eff[U, A] =
    translate(effects)(new Translate[LedgerAlgebra, U] {

      override def apply[X](op: LedgerAlgebra[X]): Eff[U, X] = op match {
        case GetStandings(customerId) =>
          wrappingInterpreter.getStandings(customerId).map(_.asInstanceOf[X])
        case AdaptStandings(customerId, amount) =>
          wrappingInterpreter
            .adaptStandings(customerId, amount)
            .map(_.asInstanceOf[X])
      }
    })

  def interpretAlgebraEffectsViaDelegate[R, U, F[_], A](delegateInterpreter: Ledger[F])(
    effects: Eff[R, A]
  )(implicit ledgerM: Member.Aux[LedgerAlgebra, R, U], effectM: F |= U): Eff[U, A] = {

    val interpreter = makeAlgebraWrapper[U, F](delegateInterpreter)
    interpretAlgebraWrapperEffects[R, U, A](interpreter)(effects)
  }

  val makeCommandWriter: Ledger[Writer[Seq[LedgerAlgebra[Any]], *]] = {
    import cats.implicits._

    new Ledger[Writer[Seq[LedgerAlgebra[Any]], *]] {
      override def getStandings(customerId: CustomerId): Writer[Seq[LedgerAlgebra[Any]], Standing] =
        Writer.tell(Seq[LedgerAlgebra[Any]](GetStandings(customerId))) *> Writer
          .value(VERSION_ZERO -> STANDING_ZERO)
      override def adaptStandings(
        customerId: CustomerId,
        amount: DeltaAmount
      ): Writer[Seq[LedgerAlgebra[Any]], Boolean] =
        Writer.tell(Seq[LedgerAlgebra[Any]](AdaptStandings(customerId, amount))) *> Writer.value(
          false
        )
    }
  }

  val makeCommandStringWriter: Ledger[Writer[Seq[String], *]] =
    new Ledger[Writer[Seq[String], *]] {
      override def getStandings(customerId: CustomerId): Writer[Seq[String], Standing] =
        makeCommandWriter.getStandings(customerId).mapWritten(_.map(_.toString))
      override def adaptStandings(
        customerId: CustomerId,
        amount: DeltaAmount
      ): Writer[Seq[String], Boolean] =
        makeCommandWriter
          .adaptStandings(customerId, amount)
          .mapWritten(_.map(_.toString))
    }
}
