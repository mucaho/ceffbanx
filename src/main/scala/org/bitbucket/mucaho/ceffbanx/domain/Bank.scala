package org.bitbucket.mucaho.ceffbanx.domain

import cats.data.Writer
import org.atnos.eff.Interpret.translate
import org.atnos.eff.{Eff, Member, Translate, |=}
import org.bitbucket.mucaho.ceffbanx.domain.Common.{
  BALANCE_ZERO,
  Balance,
  DeltaAmount,
  VERSION_ZERO
}

trait Bank[F[_]] {
  def getBalance: F[Balance]
  def adaptBalanceBy(amount: DeltaAmount): F[Boolean]
}

object Bank {
  sealed trait BankAlgebra[+A]
  case object GetBalance extends BankAlgebra[Balance]
  // can never go below a certain amount; return value indicates success
  final case class AdaptBalanceBy(amount: DeltaAmount) extends BankAlgebra[Boolean]

  object BankAlgebra {
    def getBalance[R: BankAlgebra |= *]: Eff[R, Balance] =
      Eff.send(GetBalance)
    def adaptBalanceBy[R: BankAlgebra |= *](amount: DeltaAmount): Eff[R, Boolean] =
      Eff.send(AdaptBalanceBy(amount))
  }

  def makeAlgebra[R: BankAlgebra |= *]: Bank[Eff[R, *]] = new Bank[Eff[R, *]] {
    override def getBalance: Eff[R, Balance] =
      BankAlgebra.getBalance
    override def adaptBalanceBy(amount: DeltaAmount): Eff[R, Boolean] =
      BankAlgebra.adaptBalanceBy(amount)
  }

  def makeAlgebraWrapper[R, F[_]](
    wrappedInterpreter: Bank[F]
  )(implicit ev: F |= R): Bank[Eff[R, *]] =
    new Bank[Eff[R, *]] {
      override def getBalance: Eff[R, Balance] =
        Eff.send(wrappedInterpreter.getBalance)
      override def adaptBalanceBy(amount: DeltaAmount): Eff[R, Boolean] =
        Eff.send(wrappedInterpreter.adaptBalanceBy(amount))
    }

  def interpretAlgebraWrapperEffects[R, U, A](
    wrappingInterpreter: Bank[Eff[U, *]]
  )(effects: Eff[R, A])(implicit bankM: Member.Aux[BankAlgebra, R, U]): Eff[U, A] =
    translate(effects)(new Translate[BankAlgebra, U] {

      override def apply[X](op: BankAlgebra[X]): Eff[U, X] = op match {
        case GetBalance =>
          wrappingInterpreter.getBalance.map(_.asInstanceOf[X])
        case AdaptBalanceBy(amount) =>
          wrappingInterpreter.adaptBalanceBy(amount).map(_.asInstanceOf[X])
      }
    })

  def interpretAlgebraEffectsViaDelegate[R, U, F[_], A](delegateInterpreter: Bank[F])(
    effects: Eff[R, A]
  )(implicit bankM: Member.Aux[BankAlgebra, R, U], effectM: F |= U): Eff[U, A] = {

    val interpreter = makeAlgebraWrapper[U, F](delegateInterpreter)
    interpretAlgebraWrapperEffects[R, U, A](interpreter)(effects)
  }

  val makeCommandWriter: Bank[Writer[Seq[BankAlgebra[Any]], *]] = {
    import cats.implicits._

    new Bank[Writer[Seq[BankAlgebra[Any]], *]] {
      override def getBalance: Writer[Seq[BankAlgebra[Any]], Balance] =
        Writer.tell(Seq[BankAlgebra[Any]](GetBalance)) *> Writer.value(VERSION_ZERO -> BALANCE_ZERO)
      override def adaptBalanceBy(amount: DeltaAmount): Writer[Seq[BankAlgebra[Any]], Boolean] =
        Writer.tell(Seq[BankAlgebra[Any]](AdaptBalanceBy(amount))) *> Writer
          .value(false)
    }
  }

  val makeCommandStringWriter: Bank[Writer[Seq[String], *]] =
    new Bank[Writer[Seq[String], *]] {
      override def getBalance: Writer[Seq[String], Balance] =
        makeCommandWriter.getBalance.mapWritten(_.map(_.toString))
      override def adaptBalanceBy(amount: DeltaAmount): Writer[Seq[String], Boolean] =
        makeCommandWriter.adaptBalanceBy(amount).mapWritten(_.map(_.toString))
    }
}
