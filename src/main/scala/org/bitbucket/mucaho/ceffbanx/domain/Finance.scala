package org.bitbucket.mucaho.ceffbanx
package domain

import cats.data.{EitherT, NonEmptySeq, Writer}
import org.atnos.eff.{Eff, |=}
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Finance._

import java.util.UUID
import scala.annotation.unchecked.uncheckedVariance

trait Finance[F[+_, +_]] {
  def transferMoney(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]

  def creditMoney(
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]
  def debitMoney(
    fromCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]
}

object Finance {
  trait Requestful {
    def requestId: UUID
  }

  sealed trait FinancialAlgebra[+E, +A] extends Requestful
  // queries for an action to be performed
  sealed trait FinancialCommand
      extends FinancialAlgebra[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]
  // commands that alter two customer balances
  final case class TransferMoney(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ) extends FinancialCommand
  // commands that alter customer and bank balance, as well as bank debt / savings ledger
  final case class CreditMoney(toCustomerId: CustomerId, amount: TransferAmount, requestId: UUID)
      extends FinancialCommand
  final case class DebitMoney(fromCustomerId: CustomerId, amount: TransferAmount, requestId: UUID)
      extends FinancialCommand

  // responses of an action performed
  sealed trait FinancialEvent extends Requestful
  // success responses for an action performed
  sealed trait FinancialSuccessEvent extends FinancialEvent
  // event about customer balance, send to respective customer
  final case class UpdatedCustomerBalance(customerId: CustomerId, amount: Balance, requestId: UUID)
      extends FinancialSuccessEvent
  // event about customer standings, send to respective customer
  final case class UpdatedCustomerStandings(
    customerId: CustomerId,
    amount: Standing,
    requestId: UUID
  ) extends FinancialSuccessEvent
  // event about bank balance, broadcasted to all customers (for some contrived reason)
  final case class UpdatedBankBalance(amount: Balance, requestId: UUID)
      extends FinancialSuccessEvent
  // error responses of an action performed
  sealed trait FinancialErrorEvent extends Exception with FinancialEvent
  // event about insufficient customer balance, send to respective customer
  final case class InsufficientCustomerBalance(
    customerId: CustomerId,
    amount: MissingAmount,
    requestId: UUID
  ) extends FinancialErrorEvent
  // event about overmuch customer balance, send to respective customer
  final case class OvermuchCustomerBalance(
    customerId: CustomerId,
    amount: SurplusAmount,
    requestId: UUID
  ) extends FinancialErrorEvent
  // event about overmuch customer debt, send to respective customer
  final case class OvermuchCustomerDebt(
    customerId: CustomerId,
    amount: MissingAmount,
    requestId: UUID
  ) extends FinancialErrorEvent
  // event about overmuch customer savings, send to respective customer
  final case class OvermuchCustomerSavings(
    customerId: CustomerId,
    amount: SurplusAmount,
    requestId: UUID
  ) extends FinancialErrorEvent
  // event about insufficient bank balance, broadcasted to all customers (for some contrived reason)
  final case class InsufficientBankBalance(amount: MissingAmount, requestId: UUID)
      extends FinancialErrorEvent
  // event about overmuch bank balance, broadcasted to all customers (for some contrived reason)
  final case class OvermuchBankBalance(amount: SurplusAmount, requestId: UUID)
      extends FinancialErrorEvent

  private type _financialAlgebra[R] =
    FinancialAlgebra[FinancialErrorEvent, *] |= R

  object FinancialAlgebra {
    def transferMoney[R: _financialAlgebra](
      fromCustomerId: CustomerId,
      toCustomerId: CustomerId,
      amount: TransferAmount,
      requestId: UUID
    ): Eff[R, NonEmptySeq[FinancialSuccessEvent]] =
      Eff.send(TransferMoney(fromCustomerId, toCustomerId, amount, requestId))
    def creditMoney[R: _financialAlgebra](
      toCustomerId: CustomerId,
      amount: TransferAmount,
      requestId: UUID
    ): Eff[R, NonEmptySeq[FinancialSuccessEvent]] =
      Eff.send(CreditMoney(toCustomerId, amount, requestId))
    def debitMoney[R: _financialAlgebra](
      fromCustomerId: CustomerId,
      amount: TransferAmount,
      requestId: UUID
    ): Eff[R, NonEmptySeq[FinancialSuccessEvent]] =
      Eff.send(DebitMoney(fromCustomerId, amount, requestId))
  }

  // workaround to scala 2.13.8 bug https://github.com/scala/bug/issues/12624
  final case class FinanceFactory[R: _financialAlgebra]() {
    type EffType[+E, +A] = Eff[R, A @uncheckedVariance]

    def makeAlgebra: Finance[EffType] = new Finance[EffType] {
      override def transferMoney(
        fromCustomerId: CustomerId,
        toCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): Eff[R, NonEmptySeq[FinancialSuccessEvent]] =
        Eff.send(TransferMoney(fromCustomerId, toCustomerId, amount, requestId))
      override def creditMoney(
        toCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): Eff[R, NonEmptySeq[FinancialSuccessEvent]] =
        Eff.send(CreditMoney(toCustomerId, amount, requestId))
      override def debitMoney(
        fromCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): Eff[R, NonEmptySeq[FinancialSuccessEvent]] =
        Eff.send(DebitMoney(fromCustomerId, amount, requestId))
    }

    // TODO: add makeAlgebraWrapper, interpretAlgebraWrapperEffects
  }

  type CustomerWriterType[A] = Writer[Set[CustomerId], A]
  type CustomerWrittenEffectType[+E, +A] =
    EitherT[CustomerWriterType, Throwable, A @uncheckedVariance]

  val makeCustomerWriter: Finance[CustomerWrittenEffectType] = {
    import cats.implicits._

    new Finance[CustomerWrittenEffectType] { // UNLAWFUL IMPLEMENTATION!!!
      override def transferMoney(
        fromCustomerId: CustomerId,
        toCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): EitherT[Writer[Set[CustomerId], *], Throwable, NonEmptySeq[FinancialSuccessEvent]] =
        EitherT.liftF(
          Writer.tell(Set(fromCustomerId)) *> Writer
            .tell(Set(toCustomerId)) *> Writer.value(
            NonEmptySeq.of(
              UpdatedCustomerBalance(fromCustomerId, VERSION_ZERO -> BALANCE_ZERO, requestId),
              UpdatedCustomerBalance(toCustomerId, VERSION_ZERO -> BALANCE_ZERO, requestId)
            )
          )
        )

      override def creditMoney(
        toCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): EitherT[Writer[Set[CustomerId], *], Throwable, NonEmptySeq[FinancialSuccessEvent]] =
        EitherT.liftF(
          Writer.tell(Set(toCustomerId)) *> Writer.value(
            NonEmptySeq.of(
              UpdatedBankBalance(VERSION_ZERO -> BALANCE_ZERO, requestId),
              UpdatedCustomerStandings(toCustomerId, VERSION_ZERO -> STANDING_ZERO, requestId)
            )
          )
        )

      override def debitMoney(
        fromCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): EitherT[Writer[Set[CustomerId], *], Throwable, NonEmptySeq[FinancialSuccessEvent]] =
        EitherT.liftF(
          Writer.tell(Set(fromCustomerId)) *> Writer.value(
            NonEmptySeq.of(
              UpdatedCustomerStandings(fromCustomerId, VERSION_ZERO -> STANDING_ZERO, requestId),
              UpdatedBankBalance(VERSION_ZERO -> BALANCE_ZERO, requestId)
            )
          )
        )
    }
  }

  type CommandWriterType[A] = Writer[Seq[FinancialCommand], A]
  type CommandWrittenEffectType[+E, +A] =
    EitherT[CommandWriterType, Throwable, A @uncheckedVariance]

  val makeCommandWriter: Finance[CommandWrittenEffectType] = {
    import cats.implicits._

    new Finance[CommandWrittenEffectType] { // UNLAWFUL IMPLEMENTATION!!!
      override def transferMoney(
        fromCustomerId: CustomerId,
        toCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): EitherT[Writer[Seq[FinancialCommand], *], Throwable, NonEmptySeq[FinancialSuccessEvent]] =
        EitherT.liftF(
          Writer.tell(
            Seq[FinancialCommand](TransferMoney(fromCustomerId, toCustomerId, amount, requestId))
          ) *>
            Writer.value(
              NonEmptySeq.of(
                UpdatedCustomerBalance(fromCustomerId, VERSION_ZERO -> BALANCE_ZERO, requestId),
                UpdatedCustomerBalance(toCustomerId, VERSION_ZERO -> BALANCE_ZERO, requestId)
              )
            )
        )

      override def creditMoney(
        toCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): EitherT[Writer[Seq[FinancialCommand], *], Throwable, NonEmptySeq[FinancialSuccessEvent]] =
        EitherT.liftF(
          Writer.tell(Seq[FinancialCommand](CreditMoney(toCustomerId, amount, requestId))) *>
            Writer.value(
              NonEmptySeq.of(
                UpdatedBankBalance(VERSION_ZERO -> BALANCE_ZERO, requestId),
                UpdatedCustomerStandings(toCustomerId, VERSION_ZERO -> STANDING_ZERO, requestId)
              )
            )
        )

      override def debitMoney(
        fromCustomerId: CustomerId,
        amount: TransferAmount,
        requestId: UUID
      ): EitherT[Writer[Seq[FinancialCommand], *], Throwable, NonEmptySeq[FinancialSuccessEvent]] =
        EitherT.liftF(
          Writer.tell(Seq[FinancialCommand](DebitMoney(fromCustomerId, amount, requestId))) *>
            Writer.value(
              NonEmptySeq.of(
                UpdatedCustomerStandings(fromCustomerId, VERSION_ZERO -> STANDING_ZERO, requestId),
                UpdatedBankBalance(VERSION_ZERO -> BALANCE_ZERO, requestId)
              )
            )
        )
    }
  }
}
