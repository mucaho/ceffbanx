package org.bitbucket.mucaho.ceffbanx
package domain

import cats.data.NonEmptySeq
import eu.timepit.refined.refineV
import izumi.functional.bio.Monad2
import org.bitbucket.mucaho.ceffbanx.domain.Common.{CustomerId, TRANSFER_MAX, TransferAmount}
import org.bitbucket.mucaho.ceffbanx.domain.Finance.{FinancialErrorEvent, FinancialSuccessEvent}

import java.util.UUID

// TODO: figure out a way to abstract over required capabilities (might need more than Monad2),
//  without placing additional burden on call sites

trait ProgramTemplate {
  def apply[F[+_, +_]: Monad2](
    dsl: Finance[F]
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]
}

object ProgramTemplate {
  def constructTransfer(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): ProgramTemplate = new ProgramTemplate {
    override def apply[F[+_, +_]: Monad2](
      dsl: Finance[F]
    ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]] =
      dsl.transferMoney(fromCustomerId, toCustomerId, amount, requestId)
  }

  def constructLoanAndTransfer(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): ProgramTemplate = new ProgramTemplate {
    override def apply[F[+_, +_]: Monad2](
      dsl: Finance[F]
    ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]] = {
      import cats.implicits._
      import eu.timepit.refined.auto._

      val doubleAmount: Either[String, TransferAmount] = refineV(amount * 2)
      for {
        ev0 <- dsl.creditMoney(fromCustomerId, doubleAmount.getOrElse(TRANSFER_MAX), requestId)
        ev1 <- dsl.transferMoney(fromCustomerId, toCustomerId, amount, requestId)
        ev2 <- dsl.debitMoney(toCustomerId, amount, requestId)
      } yield ev0 |+| ev1 |+| ev2
    }
  }
}
