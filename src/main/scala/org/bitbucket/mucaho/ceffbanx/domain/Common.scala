package org.bitbucket.mucaho.ceffbanx.domain

import eu.timepit.refined.api.{Refined, Validate}
import eu.timepit.refined.boolean.And
import eu.timepit.refined.collection.Size
import eu.timepit.refined.numeric.{GreaterEqual, LessEqual, Negative, NonNegative, Positive}
import eu.timepit.refined.string.{StartsWith, Trimmed}
import eu.timepit.refined.{refineMV, refineV}
import monix.newtypes.NewtypeWrapped

@SuppressWarnings(Array("org.wartremover.warts.Throw", "org.wartremover.warts.StringPlusAny"))
object Common {
  type Version = Version.Type
  object Version extends NewtypeWrapped[Long]

  type Balance = (Version, BalanceValue)
  type BalanceValue = Int Refined And[NonNegative, LessEqual[1_000_000]]

  type Standing = (Version, StandingValue)
  type StandingValue =
    Int Refined And[GreaterEqual[-100_000], LessEqual[100_000]]

  type TransferAmount = Int Refined And[Positive, LessEqual[100_000]]
  type MissingAmount = Int Refined And[Negative, GreaterEqual[-100_000]]
  type SurplusAmount = Int Refined And[Positive, LessEqual[100_000]]
  type DeltaAmount = Int Refined And[GreaterEqual[-100_000], LessEqual[100_000]]

  type CustomerId =
    String Refined And[Size[9], And[Trimmed, StartsWith["Customer"]]]

  final val VERSION_ZERO: Version = Version(0L)

  final val BALANCE_ZERO: BalanceValue = refineMV(0)
  final val BALANCE_MIN: BalanceValue = BALANCE_ZERO
  final val BALANCE_MAX: BalanceValue = refineMV(1_000_000)
  final val BALANCE_MID: BalanceValue = refineMV(500_000)
  final val STANDING_ZERO: StandingValue = refineMV(0)
  final val STANDING_MIN: StandingValue = refineMV(-100_000)
  final val STANDING_MAX: StandingValue = refineMV(100_000)

  final val TRANSFER_MAX: TransferAmount = refineMV(100_000)
  final val TRANSFER_ONE: TransferAmount = refineMV(1)
  final val DELTA_ZERO: DeltaAmount = refineMV(0)
  final val MISSING_ONE: MissingAmount = refineMV(-1)
  final val SURPLUS_ONE: SurplusAmount = refineMV(1)

  final val BANK_ID: "bank" = "bank"

  final val CUSTOMER_ID_PREFIX: "Customer" = "Customer"

  final val CUSTOMER_VALID_ID_RANGE: Seq[Char] = 'A' to 'Z'
  final val ALL_VALID_CUSTOMERS: Set[CustomerId] =
    CUSTOMER_VALID_ID_RANGE.foldLeft {
      Set.empty[CustomerId]
    } { (s: Set[CustomerId], c: Char) =>
      s +
        (refineV(CUSTOMER_ID_PREFIX + c)(Validate.alwaysPassed(())): Either[String, CustomerId])
          .getOrElse(throw new RuntimeException("Fundamental technical error occurred!"))
    }
  final val ALL_VALID_CUSTOMERS_LIST: IndexedSeq[CustomerId] =
    ALL_VALID_CUSTOMERS.toIndexedSeq

  final val CUSTOMER_ANY_ID_RANGE: Seq[Char] =
    CUSTOMER_VALID_ID_RANGE `++` ('0' until '1')
  final val ANY_CUSTOMERS: Set[CustomerId] = CUSTOMER_ANY_ID_RANGE.foldLeft {
    Set.empty[CustomerId]
  } { (s: Set[CustomerId], c: Char) =>
    s +
      (refineV(CUSTOMER_ID_PREFIX + c)(Validate.alwaysPassed(())): Either[String, CustomerId])
        .getOrElse(throw new RuntimeException("Fundamental technical error occurred!"))
  }
  final val ANY_CUSTOMERS_LIST: IndexedSeq[CustomerId] =
    ANY_CUSTOMERS.toIndexedSeq

  def newBalance(initialBalance: Balance, amount: DeltaAmount): Option[Balance] =
    newBalance(initialBalance, amount.value)

  def newBalance(initialBalance: Balance, amount: Int): Option[Balance] = {
    import eu.timepit.refined.auto._

    val (initialBalanceVersion, initialBalanceValue) = initialBalance
    val newVersion = initialBalanceVersion match {
      case Version(value) => Version(value + 1L)
    }
    (refineV(initialBalanceValue + amount): Either[String, BalanceValue]) match {
      case Right(newValue) => Some(newVersion -> newValue)
      case Left(_)         => None
    }
  }

  def newStanding(initialStanding: Standing, amount: DeltaAmount): Option[Standing] =
    newStanding(initialStanding, amount.value)

  def newStanding(initialStanding: Standing, amount: Int): Option[Standing] = {
    import eu.timepit.refined.auto._

    val (initialStandingVersion, initialStandingValue) = initialStanding
    val newVersion = initialStandingVersion match {
      case Version(value) => Version(value + 1L)
    }
    (refineV(initialStandingValue + amount): Either[String, StandingValue]) match {
      case Right(newValue) => Some(newVersion -> newValue)
      case Left(_)         => None
    }
  }

  val versionOrdering: Ordering[Version] =
    (xVersion: Version, yVersion: Version) => {
      val x = xVersion match { case Version(v) => v }
      val y = yVersion match { case Version(v) => v }

      if (x == y) {
        0
      } else if (((x > y) && (x - y <= Int.MaxValue)) || ((x < y) && (y - x > Int.MaxValue))) {
        1
      } else {
        -1
      }
    }

  def unsafeToDeltaAmount(amt: Int): DeltaAmount = {
    val deltaAmount: Either[String, DeltaAmount] = refineV(amt)
    deltaAmount.getOrElse(throw new RuntimeException("Fundamental technical error occurred!"))
  }

  def unsafeToMissingAmount(amt: Int): MissingAmount = {
    val missingAmount: Either[String, MissingAmount] = refineV(amt)
    missingAmount.getOrElse(throw new RuntimeException("Fundamental technical error occurred!"))
  }

  def unsafeToSurplusAmount(amt: Int): SurplusAmount = {
    val surplusAmount: Either[String, SurplusAmount] = refineV(amt)
    surplusAmount.getOrElse(throw new RuntimeException("Fundamental technical error occurred!"))
  }
}
