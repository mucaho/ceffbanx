package org.bitbucket.mucaho.ceffbanx.domain

import cats.data.Writer
import org.atnos.eff.Interpret.translate
import org.atnos.eff.{Eff, Member, Translate, |=}
import org.bitbucket.mucaho.ceffbanx.domain.Common.{
  BALANCE_ZERO,
  Balance,
  CustomerId,
  DeltaAmount,
  VERSION_ZERO
}

trait Account[F[_]] {
  def getBalance(customerId: CustomerId): F[Balance]
  def adaptBalanceBy(customerId: CustomerId, amount: DeltaAmount): F[Boolean]
}

object Account {
  sealed trait AccountAlgebra[+A]
  final case class GetBalance(customerId: CustomerId) extends AccountAlgebra[Balance]
  // can never go below 0; return value indicates success
  final case class AdaptBalanceBy(customerId: CustomerId, amount: DeltaAmount)
      extends AccountAlgebra[Boolean]

  object AccountAlgebra {
    def getBalance[R: AccountAlgebra |= *](customerId: CustomerId): Eff[R, Balance] =
      Eff.send(GetBalance(customerId))
    def adaptBalanceBy[R: AccountAlgebra |= *](
      customerId: CustomerId,
      amount: DeltaAmount
    ): Eff[R, Boolean] =
      Eff.send(AdaptBalanceBy(customerId, amount))
  }

  def makeAlgebra[R: AccountAlgebra |= *]: Account[Eff[R, *]] =
    new Account[Eff[R, *]] {
      override def getBalance(customerId: CustomerId): Eff[R, Balance] =
        AccountAlgebra.getBalance(customerId)
      override def adaptBalanceBy(customerId: CustomerId, amount: DeltaAmount): Eff[R, Boolean] =
        AccountAlgebra.adaptBalanceBy(customerId, amount)
    }

  def makeAlgebraWrapper[R, F[_]](
    wrappedInterpreter: Account[F]
  )(implicit ev: F |= R): Account[Eff[R, *]] =
    new Account[Eff[R, *]] {
      override def getBalance(customerId: CustomerId): Eff[R, Balance] =
        Eff.send(wrappedInterpreter.getBalance(customerId))
      override def adaptBalanceBy(customerId: CustomerId, amount: DeltaAmount): Eff[R, Boolean] =
        Eff.send(wrappedInterpreter.adaptBalanceBy(customerId, amount))
    }

  def interpretAlgebraWrapperEffects[R, U, A](
    wrappingInterpreter: Account[Eff[U, *]]
  )(effects: Eff[R, A])(implicit accountM: Member.Aux[AccountAlgebra, R, U]): Eff[U, A] =
    translate(effects)(new Translate[AccountAlgebra, U] {

      override def apply[X](op: AccountAlgebra[X]): Eff[U, X] = op match {
        case GetBalance(customerId) =>
          wrappingInterpreter.getBalance(customerId).map(_.asInstanceOf[X])
        case AdaptBalanceBy(customerId, amount) =>
          wrappingInterpreter
            .adaptBalanceBy(customerId, amount)
            .map(_.asInstanceOf[X])
      }
    })

  def interpretAlgebraEffectsViaDelegate[R, U, F[_], A](delegateInterpreter: Account[F])(
    effects: Eff[R, A]
  )(implicit accountM: Member.Aux[AccountAlgebra, R, U], effectM: F |= U): Eff[U, A] = {

    val interpreter = makeAlgebraWrapper[U, F](delegateInterpreter)
    interpretAlgebraWrapperEffects[R, U, A](interpreter)(effects)
  }

  val makeCommandWriter: Account[Writer[Seq[AccountAlgebra[Any]], *]] = {
    import cats.implicits._

    new Account[Writer[Seq[AccountAlgebra[Any]], *]] {
      override def getBalance(customerId: CustomerId): Writer[Seq[AccountAlgebra[Any]], Balance] =
        Writer.tell(Seq[AccountAlgebra[Any]](GetBalance(customerId))) *> Writer
          .value(VERSION_ZERO -> BALANCE_ZERO)
      override def adaptBalanceBy(
        customerId: CustomerId,
        amount: DeltaAmount
      ): Writer[Seq[AccountAlgebra[Any]], Boolean] =
        Writer.tell(Seq[AccountAlgebra[Any]](AdaptBalanceBy(customerId, amount))) *> Writer.value(
          false
        )
    }
  }

  val makeCommandStringWriter: Account[Writer[Seq[String], *]] =
    new Account[Writer[Seq[String], *]] {
      override def getBalance(customerId: CustomerId): Writer[Seq[String], Balance] =
        makeCommandWriter.getBalance(customerId).mapWritten(_.map(_.toString))
      override def adaptBalanceBy(
        customerId: CustomerId,
        amount: DeltaAmount
      ): Writer[Seq[String], Boolean] =
        makeCommandWriter
          .adaptBalanceBy(customerId, amount)
          .mapWritten(_.map(_.toString))
    }
}
