package org.bitbucket.mucaho.ceffbanx

import cats.data.{EitherT, NonEmptySeq}
import cats.effect.std.Random
import cats.mtl.Handle
import cats.{Applicative, FlatMap, Functor}
import eu.timepit.refined.refineV
import io.github.timwspence.cats.stm.STM
import izumi.functional.bio.Error2
import org.bitbucket.mucaho.ceffbanx.Util.EffInterpretation.{
  interpretEitherEffect,
  interpretStateEffect,
  runEffect
}
import org.bitbucket.mucaho.ceffbanx.Util.ExBitraverse
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Finance._
import org.bitbucket.mucaho.ceffbanx.domain.ProgramTemplate
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultProgramInterpreter.{
  instantiateProgramEffects,
  interpretProgramEffects
}
import tofu.higherKind.Mid
import tofu.logging.Logging

object DefaultProgramRunner {
  def generateValidCustomerId[F[_]: Functor: Random]: F[CustomerId] = {
    import cats.implicits._

    Random[F]
      .nextIntBounded(ALL_VALID_CUSTOMERS_LIST.length)
      .map((randIdx: Int) => ALL_VALID_CUSTOMERS_LIST(randIdx))
  }

  def generateAnyCustomerId[F[_]: Functor: Random]: F[CustomerId] = {
    import cats.implicits._

    Random[F]
      .nextIntBounded(ANY_CUSTOMERS_LIST.length)
      .map((randIdx: Int) => ANY_CUSTOMERS_LIST(randIdx))
  }

  def generateAnyAmount[F[_]: Functor: Random]: F[TransferAmount] = {
    import cats.implicits._

    Random[F]
      .nextIntBounded(TRANSFER_MAX.value)
      .map((amt: Int) => {
        val transferAmount: Either[String, TransferAmount] = refineV(amt)
        transferAmount.getOrElse(TRANSFER_MAX)
      })
  }

  type ProgramState[F[_], stm <: STM[F]] =
    (stm#TVar[Balance], Map[CustomerId, stm#TVar[Balance]], Map[CustomerId, stm#TVar[Standing]])

  def initializeMutableState[F[_]](stm: STM[F]): F[ProgramState[F, stm.type]] = {
    import cats.implicits._
    val accountMapTxn = ALL_VALID_CUSTOMERS.toList
      .map(customer => stm.TVar.of(VERSION_ZERO -> BALANCE_ZERO).tupleLeft(customer))
      .sequence
    val ledgerMapTxn = ALL_VALID_CUSTOMERS.toList
      .map(customer => stm.TVar.of(VERSION_ZERO -> STANDING_ZERO).tupleLeft(customer))
      .sequence

    stm.commit {
      for {
        bankTVar <- stm.TVar.of(VERSION_ZERO -> BALANCE_MAX)

        accountStateList <- accountMapTxn
        accountState = accountStateList.toMap

        ledgerStateList <- ledgerMapTxn
        ledgerState = ledgerStateList.toMap
      } yield (bankTVar, accountState, ledgerState)
    }
  }

  def readMutableState[F[_]](stm: STM[F])(
    programState: ProgramState[F, stm.type]
  ): F[(Balance, Map[CustomerId, Balance], Map[CustomerId, Standing])] = {

    import cats.implicits._

    val (bankState, accountState, ledgerState) = programState

    val ledgerStateTxn = ledgerState.view
      .mapValues(_.get)
      .toList
      .map(ExBitraverse[Tuple2].rightSequence(_))
      .sequence
    val accountStateTxn = accountState.view
      .mapValues(_.get)
      .toList
      .map(ExBitraverse[Tuple2].rightSequence(_))
      .sequence

    stm.commit {
      for {
        bankState <- bankState.get

        ledgerStateList <- ledgerStateTxn
        ledgerState = ledgerStateList.toMap

        accountStateList <- accountStateTxn
        accountState = accountStateList.toMap
      } yield (bankState, accountState, ledgerState)
    }
  }

  def processProgram[F[_]: Applicative: FlatMap: Handle[*[_], Throwable]: Logging.Make](
    stm: STM[F]
  )(
    programState: ProgramState[F, stm.type]
  )(programTemplate: ProgramTemplate): F[Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]] = {

    implicit val logging: Logging[F] = Logging.Make[F].byName("CEffBanX")

    logProgram(stm)(programState)(programTemplate) attach {
      runProgram(stm)(programState)(programTemplate)
    }
  }

  @SuppressWarnings(Array("org.wartremover.warts.StringPlusAny"))
  private def logProgram[F[_]](
    stm: STM[F]
  )(programState: ProgramState[F, stm.type])(programTemplate: ProgramTemplate)(implicit
    M: FlatMap[F],
    L: Logging[F]
  ): Mid[F, Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]] = {

    implicit val errorInstance: Error2[CommandWrittenEffectType] =
      new Util.CatsBIOImplicitInstances.Factory[
        EitherT[CommandWriterType, Throwable, *]
      ].errorInstance
    val writtenProgram = programTemplate(makeCommandWriter)
    val (commands, _)
      : (Seq[FinancialCommand], Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]) =
      writtenProgram.value.run

    wrappedComputation => {
      import tofu.syntax.monadic._

      for {
        startState <- readMutableState(stm)(programState)
        hash = programTemplate.hashCode
        _ <- L.info(s"Pre-program #$hash state: $startState")
        _ <- L.info(s"Program #$hash commands: $commands}")
        events <- wrappedComputation
        _ <- L.info(s"Program #$hash events: $events")
        endState <- readMutableState(stm)(programState)
        _ <- L.info(s"Post-program #$hash state: $endState")
      } yield events
    }
  }

  // TODO: consider accumulating all errors into list
  //  -> e.g. accumulate with eitherEffect.runEitherCombined
  //  -> error type must be non-empty semi-groupable collection extending throwable
  @SuppressWarnings(Array("org.wartremover.warts.Return"))
  private def runProgram[F[_]](
    stm: STM[F]
  )(programState: ProgramState[F, stm.type])(programTemplate: ProgramTemplate)(implicit
    A: Applicative[F],
    H: Handle[F, Throwable]
  ): F[Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]] = {

    import cats.implicits._
    import stm._

    val (bankState, accountState, ledgerState) = programState

    implicit val errorInstance: Error2[CustomerWrittenEffectType] =
      new Util.CatsBIOImplicitInstances.Factory[
        EitherT[CustomerWriterType, Throwable, *]
      ].errorInstance
    val writtenProgram = programTemplate(makeCustomerWriter)
    val (customers, eitherOut)
      : (Set[CustomerId], Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]) =
      writtenProgram.value.run

    val validCustomers = customers.subsetOf(ALL_VALID_CUSTOMERS)
    val needsBankBalance =
      eitherOut.isRight && eitherOut.exists(_.exists(_.isInstanceOf[UpdatedBankBalance]))

    if (!validCustomers) {
      return A.pure(
        Either.left(new RuntimeException(s"Invalid customerIds: ${customers.toString}!"))
      )
    }

    val accountStateView = accountState.view.filterKeys(customers).toMap
    val ledgerStateView = ledgerState.view.filterKeys(customers).toMap

    val ledgerStateTxn = ledgerStateView.view
      .mapValues(_.get)
      .toList
      .map(ExBitraverse[Tuple2].rightSequence(_))
      .sequence

    val accountStateTxn = accountStateView.view
      .mapValues(_.get)
      .toList
      .map(ExBitraverse[Tuple2].rightSequence(_))
      .sequence

    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    val out = stm.commit {
      for {
        oldBankBalance <-
          if (needsBankBalance) {
            bankState.get
          } else {
            stm.pure(VERSION_ZERO -> BALANCE_ZERO)
          }

        oldAccountsList <- accountStateTxn
        oldAccounts = oldAccountsList.toMap

        oldLedgersList <- ledgerStateTxn
        oldLedgers = oldLedgersList.toMap

        oldProgramState = (oldBankBalance, oldAccounts, oldLedgers)

        interpretedProgram =
          interpretStateEffect { oldProgramState } {
            interpretEitherEffect {
              interpretProgramEffects {
                instantiateProgramEffects(programTemplate) // program.apply
              }
            }
          }
        (eitherResult, newProgramState) = runEffect(interpretedProgram)

        _ <- eitherResult match {
          case Left(value)  => stm.abort(value)
          case Right(value) => stm.pure(value)
        }

        (newBankBalance, newAccounts, newLedgers) = newProgramState

        newAccountsFiltered = newAccounts.view.filterKeys(customers).toMap
        newLedgersFiltered = newLedgers.view.filterKeys(customers).toMap

        _ <-
          if (needsBankBalance) {
            bankState.set(newBankBalance)
          } else {
            stm.unit
          }

        _ <- newAccountsFiltered.foldLeft {
          stm.pure(())
        } { (txn: Txn[Unit], value: (CustomerId, Balance)) =>
          txn *> accountState.getOrElse(value._1, null).set(value._2)
        }

        _ <- newLedgersFiltered.foldLeft {
          stm.pure(())
        } { (txn: Txn[Unit], value: (CustomerId, Standing)) =>
          txn *> ledgerState.getOrElse(value._1, null).set(value._2)
        }
      } yield eitherResult
    }

    H.handleWith { out } { (e: Throwable) => A.pure(Either.left(e)) }
  }
}
