package org.bitbucket.mucaho.ceffbanx
package interpreter

import cats.Monad
import cats.data.{State, Writer}
import cats.mtl.{Stateful, Tell}
import org.atnos.eff.Interpret.translate
import org.atnos.eff._
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Ledger
import org.bitbucket.mucaho.ceffbanx.domain.Ledger._

object DefaultLedgerInterpreter {
  private def interpretLedgerState[F[_]](implicit
    monad: Monad[F],
    state: Stateful[F, Map[CustomerId, Standing]],
    writer: Tell[F, String]
  ): Ledger[F] = new Ledger[F] {
    override def getStandings(customerId: CustomerId): F[Standing] =
      state.inspect(_.getOrElse(customerId, VERSION_ZERO -> STANDING_ZERO))
    override def adaptStandings(customerId: CustomerId, amount: DeltaAmount): F[Boolean] = {
      import cats.implicits._

      for {
        currentStanding <- state.inspect(_.getOrElse(customerId, VERSION_ZERO -> STANDING_ZERO))
        updated <- newStanding(currentStanding, amount) match {
          case Some(standing) =>
            for {
              _ <- state.modify((map: Map[CustomerId, Standing]) =>
                map.updated(customerId, standing)
              )
              _ <- writer.tell(
                s"Current standings of s${customerId.toString} changed by s${amount.toString}!"
              )
            } yield true
          case None =>
            monad.pure(false)
        }
      } yield updated
    }
  }

  private def doInterpretLedgerEffects[R, U, F[_], A](effects: Eff[R, A])(implicit
    ledgerM: Member.Aux[LedgerAlgebra, R, U],
    effectM: F |= U,
    monadM: Monad[F],
    tellM: Tell[F, String],
    statefulM: Stateful[F, Map[CustomerId, Standing]]
  ): Eff[U, A] = {

    val interpreter = makeAlgebraWrapper[U, F](interpretLedgerState[F])
    interpretAlgebraWrapperEffects[R, U, A](interpreter)(effects)
  }

  private def wrapAndInterpretLedgerEffects[R, U, A](effects: Eff[R, A])(implicit
    ledgerM: Member.Aux[LedgerAlgebra, R, U],
    stateM: State[Map[CustomerId, Standing], *] |= U,
    writerM: Writer[String, *] |= U
  ): Eff[U, A] = {

    import Util.EffMtlImplicitInstances._

    type RX = Fx.prepend[Eff[U, *], R]
    type UX = Fx.prepend[Eff[U, *], U]

    val extendedEffectsWithLedger = Eff.effInto[R, RX, A](effects)

    val extendedEffectsWithoutLedger =
      doInterpretLedgerEffects[RX, UX, Eff[U, *], A](extendedEffectsWithLedger)

    translate(extendedEffectsWithoutLedger)(new Translate[Eff[U, *], U] {
      override def apply[X](eff: Eff[U, X]): Eff[U, X] = eff
    })
  }

  def interpretLedgerEffects[R, U, A](effects: Eff[R, A])(implicit
    ledgerM: Member.Aux[LedgerAlgebra, R, U],
    stateM: State[Map[CustomerId, Standing], *] |= U
  ): Eff[U, A] = {

    type RX = Fx.prepend[Writer[String, *], R]
    type UX = Fx.prepend[Writer[String, *], U]

    val extendedEffectsWithLedger = Eff.effInto[R, RX, A](effects)

    val extendedEffectsWithoutLedger =
      wrapAndInterpretLedgerEffects[RX, UX, A](extendedEffectsWithLedger)

    writer
      .runWriter[UX, U, String, A, Nothing](extendedEffectsWithoutLedger)
      .map((tuple: (A, List[String])) => tuple._1)
  }
}
