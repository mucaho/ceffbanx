package org.bitbucket.mucaho.ceffbanx
package interpreter

import cats.data.{NonEmptySeq, Writer}
import org.atnos.eff.Interpret.translate
import org.atnos.eff._
import org.bitbucket.mucaho.ceffbanx.domain.Account.AccountAlgebra
import org.bitbucket.mucaho.ceffbanx.domain.Account.AccountAlgebra.{
  adaptBalanceBy => adaptCustomerBalanceBy,
  getBalance => getCustomerBalance
}
import org.bitbucket.mucaho.ceffbanx.domain.Bank.BankAlgebra
import org.bitbucket.mucaho.ceffbanx.domain.Bank.BankAlgebra.{
  adaptBalanceBy => adaptBankBalanceBy,
  getBalance => getBankBalance
}
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Finance._
import org.bitbucket.mucaho.ceffbanx.domain.Ledger._
import org.bitbucket.mucaho.ceffbanx.domain.Ledger.LedgerAlgebra.{adaptStandings, getStandings}

object DefaultFinanceInterpreter {
  private def doInterpretFinancialEffects[R, U, A](effects: Eff[R, A])(implicit
    financialM: Member.Aux[FinancialAlgebra[FinancialErrorEvent, *], R, U],
    eitherM: Either[Throwable, *] |= U,
    bankM: BankAlgebra |= U,
    accountM: AccountAlgebra |= U,
    ledgerM: LedgerAlgebra |= U
  ): Eff[U, A] =
    translate(effects)(new Translate[FinancialAlgebra[FinancialErrorEvent, *], U] {
      import eu.timepit.refined.auto._

      @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
      override def apply[X](op: FinancialAlgebra[FinancialErrorEvent, X]): Eff[U, X] = op match {
        case TransferMoney(fromCustomerId, toCustomerId, amount, requestId) =>
          for {
            decreased <- adaptCustomerBalanceBy(fromCustomerId, unsafeToDeltaAmount(-amount))
            fromCustBal <- getCustomerBalance(fromCustomerId)
            (_, fromCustBalValue) = fromCustBal

            increased <- adaptCustomerBalanceBy(toCustomerId, unsafeToDeltaAmount(+amount))
            toCustBal <- getCustomerBalance(toCustomerId)
            (_, toCustBalValue) = toCustBal

            events <-
              if (decreased && increased) {
                either
                  .right[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                    NonEmptySeq.of(
                      UpdatedCustomerBalance(fromCustomerId, fromCustBal, requestId),
                      UpdatedCustomerBalance(toCustomerId, toCustBal, requestId)
                    )
                  )
              } else if (!decreased) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  InsufficientCustomerBalance(
                    fromCustomerId,
                    unsafeToMissingAmount(fromCustBalValue - amount),
                    requestId
                  )
                )
              } else if (!increased) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  OvermuchCustomerBalance(
                    toCustomerId,
                    unsafeToSurplusAmount(amount - BALANCE_MAX + toCustBalValue),
                    requestId
                  )
                )
              } else {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  new RuntimeException("Fundamental technical error occurred!")
                )
              }
          } yield events.asInstanceOf[X]

        case CreditMoney(toCustomerId, amount, requestId) =>
          for {
            credited <- adaptStandings(toCustomerId, unsafeToDeltaAmount(-amount))
            standings <- getStandings(toCustomerId)
            (_, standingsValue) = standings

            decreased <- adaptBankBalanceBy(unsafeToDeltaAmount(-amount))
            bankBalance <- getBankBalance
            (_, bankBalanceValue) = bankBalance

            increased <- adaptCustomerBalanceBy(toCustomerId, unsafeToDeltaAmount(+amount))
            toCustBal <- getCustomerBalance(toCustomerId)
            (_, toCustBalValue) = toCustBal

            events <-
              if (credited && decreased && increased) {
                either
                  .right[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                    NonEmptySeq.of(
                      UpdatedCustomerBalance(toCustomerId, toCustBal, requestId),
                      UpdatedBankBalance(bankBalance, requestId),
                      UpdatedCustomerStandings(toCustomerId, standings, requestId)
                    )
                  )
              } else if (!credited) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  OvermuchCustomerDebt(
                    toCustomerId,
                    unsafeToMissingAmount(standingsValue - STANDING_MIN - amount),
                    requestId
                  )
                )
              } else if (!decreased) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  InsufficientBankBalance(
                    unsafeToMissingAmount(bankBalanceValue - amount),
                    requestId
                  )
                )
              } else if (!increased) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  OvermuchCustomerBalance(
                    toCustomerId,
                    unsafeToSurplusAmount(amount - BALANCE_MAX + toCustBalValue),
                    requestId
                  )
                )
              } else {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  new RuntimeException("Fundamental technical error occurred!")
                )
              }
          } yield events.asInstanceOf[X]

        case DebitMoney(fromCustomerId, amount, requestId) =>
          for {
            decreased <- adaptCustomerBalanceBy(fromCustomerId, unsafeToDeltaAmount(-amount))
            fromCustBal <- getCustomerBalance(fromCustomerId)
            (_, fromCustBalValue) = fromCustBal

            debited <- adaptStandings(fromCustomerId, unsafeToDeltaAmount(+amount))
            standings <- getStandings(fromCustomerId)
            (_, standingsValue) = standings

            increased <- adaptBankBalanceBy(unsafeToDeltaAmount(+amount))
            bankBalance <- getBankBalance
            (_, bankBalanceValue) = bankBalance

            events <-
              if (decreased && debited && increased) {
                either
                  .right[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                    NonEmptySeq.of(
                      UpdatedCustomerBalance(fromCustomerId, fromCustBal, requestId),
                      UpdatedBankBalance(bankBalance, requestId),
                      UpdatedCustomerStandings(fromCustomerId, standings, requestId)
                    )
                  )
              } else if (!decreased) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  InsufficientCustomerBalance(
                    fromCustomerId,
                    unsafeToMissingAmount(fromCustBalValue - amount),
                    requestId
                  )
                )
              } else if (!debited) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  OvermuchCustomerSavings(
                    fromCustomerId,
                    unsafeToSurplusAmount(amount - STANDING_MAX + standingsValue),
                    requestId
                  )
                )
              } else if (!increased) {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  OvermuchBankBalance(
                    unsafeToSurplusAmount(amount - BALANCE_MAX + bankBalanceValue),
                    requestId
                  )
                )
              } else {
                either.left[U, Throwable, NonEmptySeq[FinancialSuccessEvent]](
                  new RuntimeException("Fundamental technical error occurred!")
                )
              }
          } yield events.asInstanceOf[X]
      }
    })

  private def doInterpretFinancialLogEffect[R, U, A](effects: Eff[R, A])(implicit
    financialM: Member.Aux[FinancialAlgebra[FinancialErrorEvent, *], R, U],
    writerM: Writer[String, *] |= U
  ): Eff[U, A] =
    translate(effects)(new Translate[FinancialAlgebra[FinancialErrorEvent, *], U] {

      @SuppressWarnings(
        Array("org.wartremover.warts.AsInstanceOf", "org.wartremover.warts.StringPlusAny")
      )
      override def apply[X](op: FinancialAlgebra[FinancialErrorEvent, X]): Eff[U, X] = op match {
        case TransferMoney(fromCustomerId, toCustomerId, amount, requestId) =>
          writer.tell(s"Transfer $amount from $fromCustomerId to $toCustomerId") >>
            Eff.pure[U, X](
              NonEmptySeq
                .of(
                  UpdatedCustomerBalance(fromCustomerId, VERSION_ZERO -> BALANCE_ZERO, requestId),
                  UpdatedCustomerBalance(toCustomerId, VERSION_ZERO -> BALANCE_ZERO, requestId)
                )
                .asInstanceOf[X]
            )
        case DebitMoney(fromCustomerId, amount, requestId) =>
          writer.tell(s"Debit $amount from $fromCustomerId to bank") >>
            Eff.pure[U, X](
              NonEmptySeq
                .of(
                  UpdatedCustomerStandings(
                    fromCustomerId,
                    VERSION_ZERO -> STANDING_ZERO,
                    requestId
                  ),
                  UpdatedBankBalance(VERSION_ZERO -> BALANCE_ZERO, requestId)
                )
                .asInstanceOf[X]
            )
        case CreditMoney(toCustomerId, amount, requestId) =>
          writer.tell(s"Credit $amount from bank to $toCustomerId") >>
            Eff.pure[U, X](
              NonEmptySeq
                .of(
                  UpdatedBankBalance(VERSION_ZERO -> BALANCE_ZERO, requestId),
                  UpdatedCustomerStandings(toCustomerId, VERSION_ZERO -> STANDING_ZERO, requestId)
                )
                .asInstanceOf[X]
            )
      }
    })

  def interpretFinancialEffects[R, U, A](effects: Eff[R, A])(implicit
    financialM: Member.Aux[FinancialAlgebra[FinancialErrorEvent, *], R, U],
    eitherM: Either[Throwable, *] |= U,
    bankM: BankAlgebra |= U,
    accountM: AccountAlgebra |= U,
    ledgerM: LedgerAlgebra |= U
  ): Eff[U, A] = {

    val interpretFirst = doInterpretFinancialEffects(effects)

    type RX = Fx.prepend[Writer[String, *], R]
    type UX = Fx.prepend[Writer[String, *], U]
    val interpretTemp = doInterpretFinancialLogEffect(Eff.effInto[R, RX, A](effects))
    val interpretSecond =
      writer.runWriter[UX, U, String, A, Nothing](interpretTemp).map(_._1)

    interpretFirst.map2(interpretSecond)((a, _) => a)
  }
}
