package org.bitbucket.mucaho.ceffbanx.interpreter

import cats.data.State
import cats.~>
import org.atnos.eff.Interpret.transform
import org.atnos.eff.{Eff, Member}
import org.bitbucket.mucaho.ceffbanx.domain.Account._
import org.bitbucket.mucaho.ceffbanx.domain.Common._

object DefaultAccountInterpreter {

  private val compilerForAccountEffects: AccountAlgebra ~> State[Map[CustomerId, Balance], *] =
    new (AccountAlgebra ~> State[Map[CustomerId, Balance], *]) {
      @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
      override def apply[A](op: AccountAlgebra[A]): State[Map[CustomerId, Balance], A] = op match {

        case GetBalance(customerId) =>
          State.inspect((map: Map[CustomerId, Balance]) =>
            map
              .getOrElse(customerId, VERSION_ZERO -> BALANCE_ZERO)
              .asInstanceOf[A]
          )
        case AdaptBalanceBy(customerId, amount) =>
          State { (map: Map[CustomerId, Balance]) =>
            val currentBalance =
              map.getOrElse(customerId, VERSION_ZERO -> BALANCE_ZERO)
            newBalance(currentBalance, amount) match {
              case Some(balance) =>
                map.updated(customerId, balance) -> true.asInstanceOf[A]
              case None =>
                map -> false.asInstanceOf[A]
            }
          }
      }
    }

  def transformAccountEffects[R, BR, U, A](eff: Eff[R, A])(implicit
    accountM: Member.Aux[AccountAlgebra, R, U],
    stateM: Member.Aux[State[Map[CustomerId, Balance], *], BR, U]
  ): Eff[BR, A] = {
    transform(eff, compilerForAccountEffects)
  }
}
