package org.bitbucket.mucaho.ceffbanx
package interpreter

import cats.data.{NonEmptySeq, State}
import izumi.functional.bio.Monad2
import org.atnos.eff.{Eff, Fx, Fx2}
import org.bitbucket.mucaho.ceffbanx.Util.EffBIOImplicitInstances
import org.bitbucket.mucaho.ceffbanx.Util.EffInterpretation.translateLensState
import org.bitbucket.mucaho.ceffbanx.domain.Account._
import org.bitbucket.mucaho.ceffbanx.domain.Bank.BankAlgebra
import org.bitbucket.mucaho.ceffbanx.domain.Common.{Balance, CustomerId, Standing}
import org.bitbucket.mucaho.ceffbanx.domain.Finance._
import org.bitbucket.mucaho.ceffbanx.domain.Ledger._
import org.bitbucket.mucaho.ceffbanx.domain.ProgramTemplate
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultAccountInterpreter.transformAccountEffects
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultBankInterpreter.interpretBankEffects
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultFinanceInterpreter.interpretFinancialEffects
import org.bitbucket.mucaho.ceffbanx.interpreter.DefaultLedgerInterpreter.interpretLedgerEffects

import scala.annotation.unchecked.uncheckedVariance

object DefaultProgramInterpreter {

  private type ProgramState =
    (Balance, Map[CustomerId, Balance], Map[CustomerId, Standing])

  // format: off
  private type EffectStackIn = Fx.fx8[
    FinancialAlgebra[FinancialErrorEvent, *],
    BankAlgebra,
    AccountAlgebra,
    LedgerAlgebra,
    State[Balance, *],
    State[Map[CustomerId, Standing], *],
    State[ProgramState, *],
    Either[Throwable, *]
  ]
  // format: on

  @annotation.nowarn("cat=unused")
  private type EffectStackOut =
    Fx2[State[ProgramState, *], Either[Throwable, *]]
  private type BiEffIn[+E, +A] = Eff[EffectStackIn, A @uncheckedVariance]

  def instantiateProgramEffects(
    programTemplate: ProgramTemplate
  ): Eff[EffectStackIn, NonEmptySeq[FinancialSuccessEvent]] = {
    implicit val monadInstance: Monad2[BiEffIn] =
      new EffBIOImplicitInstances.Factory[EffectStackIn].monadInstance

    programTemplate.apply(FinanceFactory[EffectStackIn]().makeAlgebra)
  }

  def interpretProgramEffects(
    programEffects: Eff[EffectStackIn, NonEmptySeq[FinancialSuccessEvent]]
  ): Eff[EffectStackOut, NonEmptySeq[FinancialSuccessEvent]] = {

    // run highest-level layer instructions first
    val highestInterpreted =
      interpretFinancialEffects {
        programEffects
      }

    // run lower-level layer instructions second
    val lowerInterpreted =
      interpretLedgerEffects {
        transformAccountEffects {
          interpretBankEffects {
            highestInterpreted
          }
        }
      }

    // translate each lower-layer state into one big application state
    val lowestInterpreted =
      translateLensState {
        translateLensState {
          translateLensState {
            lowerInterpreted
          } { (s: ProgramState) => s._1 } { (s: ProgramState, t: Balance) =>
            (t, s._2, s._3)
          }
        } { (s: ProgramState) => s._2 } { (s: ProgramState, t: Map[CustomerId, Balance]) =>
          (s._1, t, s._3)
        }
      } { (s: ProgramState) => s._3 } { (s: ProgramState, t: Map[CustomerId, Standing]) =>
        (s._1, s._2, t)
      }

    lowestInterpreted
  }

  // TODO: alternatively, translate outgoing Eff effects into MTL effects, with something like this:
  //  def interpretProgramEffectsAlt[F[_] : Stateful[*[_], ProgramState] : Raise[*[_], Throwable]](programTemplate: ProgramTemplate): F[NonEmptySeq[FinancialSuccessEvent]] = {
  //    val x = instantiateProgramEffects(programTemplate)
  //    val y = interpretProgramEffects(x)
  //    import Util.EffMtlImplicitInstances._
  //    y
  //  }
}
