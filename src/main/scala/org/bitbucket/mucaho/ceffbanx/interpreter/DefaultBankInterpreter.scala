package org.bitbucket.mucaho.ceffbanx
package interpreter

import effekt.Control
import org.atnos.eff.Interpret.translate
import org.atnos.eff.{Eff, Member, Translate, |=}
import org.bitbucket.mucaho.ceffbanx.domain.Bank
import org.bitbucket.mucaho.ceffbanx.domain.Bank._
import org.bitbucket.mucaho.ceffbanx.domain.Common._

object DefaultBankInterpreter {

  private def interpretBankState(implicit state: effekt.effects.State[Balance]): Bank[Control] =
    new Bank[Control] {

      override def getBalance: Control[Balance] = state.get()

      override def adaptBalanceBy(amount: DeltaAmount): Control[Boolean] =
        for {
          currentBalance <- state.get()
          updated <- newBalance(currentBalance, amount) match {
            case Some(balance) =>
              for {
                _ <- state.put(balance)
              } yield true
            case None =>
              effekt.pure(false)
          }
        } yield updated
    }

  def interpretBankEffects[R, U, A](effects: Eff[R, A])(implicit
    bankM: Member.Aux[BankAlgebra, R, U],
    stateM: cats.data.State[Balance, *] |= U
  ): Eff[U, A] = {

    // TODO: lift Control effect into Eff effect instead of running it
    //  -> trait BankEffekt extends Bank[Control]
    translate(effects)(new Translate[BankAlgebra, U] {
      @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
      override def apply[X](op: BankAlgebra[X]): Eff[U, X] = op match {
        case GetBalance =>
          Eff.send(cats.data.State { (balance: Balance) =>
            {

              val program: Control[(Balance, X)] = effekt.effects.State
                .state(balance) { state: effekt.effects.State[Balance] =>
                  interpretBankState(state).getBalance
                    .flatMap(result => state.get().map(value => (value, result.asInstanceOf[X])))
                }
              program.run()
            }
          })

        case AdaptBalanceBy(amount) =>
          Eff.send(cats.data.State { (balance: Balance) =>
            {

              val program: Control[(Balance, X)] = effekt.effects.State
                .state(balance) { state: effekt.effects.State[Balance] =>
                  interpretBankState(state)
                    .adaptBalanceBy(amount)
                    .flatMap(result => state.get().map(value => (value, result.asInstanceOf[X])))
                }
              program.run()
            }
          })
      }
    })
  }
}
