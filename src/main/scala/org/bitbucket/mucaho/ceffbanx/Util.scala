package org.bitbucket.mucaho.ceffbanx

import cats.data.{Reader, State, Writer}
import cats.mtl._
import cats.{Applicative, Bitraverse, Eval, Functor, Monad}
import izumi.functional.bio.{Error2, Monad2}
import org.atnos.eff.Eff.{EffApplicative, EffMonad}
import org.atnos.eff.Interpret.translate
import org.atnos.eff._

import scala.annotation.unchecked.uncheckedVariance
import scala.util.{Failure, Success, Try}

@SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
object Util {

  trait ExBitraverse[F[_, _]] extends Bitraverse[F] {
    def rightSequence[H[_], A, B](fahb: F[A, H[B]])(implicit H: Applicative[H]): H[F[A, B]] =
      bitraverse(fahb)(H.pure, identity)
  }

  object ExBitraverse {
    def apply[F[_, _]](implicit instance: Bitraverse[F]): ExBitraverse[F] =
      new ExBitraverse[F] {
        override def bitraverse[G[_], A, B, C, D](fab: F[A, B])(f: A => G[C], g: B => G[D])(implicit
          evidence$1: Applicative[G]
        ): G[F[C, D]] =
          instance.bitraverse(fab)(f, g)
        override def bifoldLeft[A, B, C](fab: F[A, B], c: C)(f: (C, A) => C, g: (C, B) => C): C =
          instance.bifoldLeft(fab, c)(f, g)
        override def bifoldRight[A, B, C](fab: F[A, B], c: Eval[C])(
          f: (A, Eval[C]) => Eval[C],
          g: (B, Eval[C]) => Eval[C]
        ): Eval[C] =
          instance.bifoldRight(fab, c)(f, g)
      }
  }

  object EffInterpretation {
    def interpretEitherEffect[R, U, A](effects: Eff[R, A])(implicit
      eitherM: Member.Aux[Either[Throwable, *], R, U]
    ): Eff[U, Either[Throwable, A]] =
      either.runEither(effects)

    def interpretStateEffect[R, U, A, S](initialState: S)(effects: Eff[R, A])(implicit
      stateM: Member.Aux[State[S, *], R, U]
    ): Eff[U, (A, S)] =
      state.runState(initialState)(effects)

    def interpretWriterEffect[R, U, A, L](effects: Eff[R, A])(implicit
      writerM: Member.Aux[Writer[L, *], R, U]
    ): Eff[U, (A, List[L])] =
      writer.runWriter(effects)

    def runEffect[A](eff: Eff[NoFx, A]): A = Eff.run(eff)

    def translateLensState[R, U, A, S, T](
      effects: Eff[R, A]
    )(getter: S => T)(setter: (S, T) => S)(implicit
      smallerStateM: Member.Aux[State[T, *], R, U],
      biggerStateM: State[S, *] |= U
    ): Eff[U, A] = {

      translate(effects)(new Translate[State[T, *], U] {
        override def apply[X](smallerState: State[T, X]): Eff[U, X] =
          Eff.send {
            State { biggerState: S =>
              val (smallerStateNext, x) =
                smallerState.run(getter(biggerState)).value
              (setter(biggerState, smallerStateNext), x)
            }
          }
      })
    }
  }

  object EffMtlImplicitInstances {
    implicit def effFunctor[R]: Functor[Eff[R, *]] = effApplicative

    implicit def effApplicative[R]: Applicative[Eff[R, *]] = EffApplicative[R]

    implicit def effMonad[R]: Monad[Eff[R, *]] = EffMonad[R]

    implicit def effStateful[R, A](implicit member: State[A, *] |= R): Stateful[Eff[R, *], A] =
      new Stateful[Eff[R, *], A] {
        override def monad: Monad[Eff[R, *]] = effMonad
        override def get: Eff[R, A] = org.atnos.eff.state.get
        override def set(s: A): Eff[R, Unit] = org.atnos.eff.state.put(s)
      }

    implicit def effTell[R, A](implicit member: Writer[A, *] |= R): Tell[Eff[R, *], A] =
      new Tell[Eff[R, *], A] {
        override def functor: Functor[Eff[R, *]] = effFunctor
        override def tell(l: A): Eff[R, Unit] = org.atnos.eff.writer.tell(l)
      }

    implicit def effAsk[R, A](implicit member: Reader[A, *] |= R): Ask[Eff[R, *], A] =
      new Ask[Eff[R, *], A] {
        override def applicative: Applicative[Eff[R, *]] = effApplicative
        override def ask[E2 >: A]: Eff[R, E2] =
          org.atnos.eff.reader.ask[R, A].asInstanceOf[Eff[R, E2]]
      }

    implicit def effLocal[R, A](implicit member: Reader[A, *] |= R): Local[Eff[R, *], A] =
      new Local[Eff[R, *], A] {
        override def applicative: Applicative[Eff[R, *]] = effApplicative
        override def ask[E2 >: A]: Eff[R, E2] =
          org.atnos.eff.reader.ask[R, A].asInstanceOf[Eff[R, E2]]
        override def local[A2](fa: Eff[R, A2])(f: A => A): Eff[R, A2] =
          org.atnos.eff.reader.local[R, A, A2](a => f(a).asInstanceOf[A2])
      }

    implicit def effRaise[R, E](implicit member: Either[E, *] |= R): Raise[Eff[R, *], E] =
      new Raise[Eff[R, *], E] {
        override def functor: Functor[Eff[R, *]] = effFunctor
        override def raise[E2 <: E, A](e: E2): Eff[R, A] =
          org.atnos.eff.either.left(e.asInstanceOf[E])
      }

    implicit def effHandle[R, E](implicit member: Either[E, *] /= R): Handle[Eff[R, *], E] =
      new Handle[Eff[R, *], E] {
        override def applicative: Applicative[Eff[R, *]] = effApplicative
        override def handleWith[A](fa: Eff[R, A])(f: E => Eff[R, A]): Eff[R, A] =
          org.atnos.eff.either.catchLeft(fa)(f)
        override def raise[E2 <: E, A](e: E2): Eff[R, A] =
          org.atnos.eff.either.left(e.asInstanceOf[E])
      }
  }

  object CatsBIOImplicitInstances {
    // workaround to scala 2.13.8 bug https://github.com/scala/bug/issues/12624
    class Factory[F[_]] {
      type BiF[+E, +A] = F[A @uncheckedVariance]

      implicit def monadInstance(implicit m: Monad[F]): Monad2[BiF] =
        new Monad2[BiF] {
          override def flatMap[R, E, A, B](r: BiF[E, A])(f: A => BiF[E, B]): BiF[E, B] =
            m.flatMap(r)(f)
          override def pure[A](a: A): BiF[Nothing, A] = m.pure(a)
        }

      implicit def errorInstance(implicit m: Monad[F], h: Handle[F, Throwable]): Error2[BiF] =
        new Error2[BiF] {
          import cats.implicits._
          import cats.mtl.implicits._

          override def catchAll[R, E, A, E2](r: F[A])(f: E => F[A]): F[A] =
            r.handleWith((e: Throwable) => f(e.asInstanceOf[E]))

          override def flatMap[R, E, A, B](r: F[A])(f: A => F[B]): F[B] =
            r.flatMap(f)

          override def fail[E](v: => E): F[Nothing] =
            v.asInstanceOf[Throwable].raise

          override def fromEither[E, V](effect: => Either[E, V]): F[V] =
            effect match {
              case Right(value) => value.pure(h.applicative)
              case Left(value)  => value.asInstanceOf[Throwable].raise
            }

          override def fromOption[E, A](errorOnNone: => E)(effect: => Option[A]): F[A] =
            effect match {
              case Some(value) => value.pure(h.applicative)
              case None        => errorOnNone.asInstanceOf[Throwable].raise
            }

          override def fromTry[A](effect: => Try[A]): F[A] =
            effect match {
              case Success(value)     => value.pure(h.applicative)
              case Failure(exception) => exception.raise
            }

          override def guarantee[R, E, A](f: F[A], cleanup: F[Unit]): F[A] =
            f.handleWith((_: Throwable) => cleanup.asInstanceOf[F[A]])

          override def pure[A](a: A): F[A] = a.pure(h.applicative)
        }
    }
  }

  object EffBIOImplicitInstances {
    // workaround to scala 2.13.8 bug https://github.com/scala/bug/issues/12624
    class Factory[R] {
      type BiEff[+E, +A] = Eff[R, A @uncheckedVariance]

      implicit def monadInstance: Monad2[BiEff] = new Monad2[BiEff] {
        override def flatMap[Env, E, A, B](r: Eff[R, A])(f: A => Eff[R, B]): Eff[R, B] =
          r.flatMap(f)
        override def pure[A](a: A): Eff[R, A] = Eff.pure(a)
      }

      implicit def eitherErrorInstance[H[_]](implicit
        member: Either[Throwable, *] /= R
      ): Error2[BiEff] = new Error2[BiEff] {
        override def catchAll[Env, E, A, E2](r: Eff[R, A])(f: E => Eff[R, A]): Eff[R, A] =
          either.catchLeft(r)((e: Throwable) => f(e.asInstanceOf[E]))(member)

        override def flatMap[Env, E, A, B](r: Eff[R, A])(f: A => Eff[R, B]): Eff[R, B] =
          r.flatMap(f)

        override def fail[E](v: => E): Eff[R, Nothing] =
          either.left(v.asInstanceOf[Throwable])(member)

        override def fromEither[E, V](effect: => Either[E, V]): Eff[R, V] =
          either.fromEither(effect.left.map(_.asInstanceOf[Throwable]))(member)

        override def fromOption[E, A](errorOnNone: => E)(effect: => Option[A]): Eff[R, A] =
          either.optionEither(effect, errorOnNone.asInstanceOf[Throwable])(member)

        override def fromTry[A](effect: => Try[A]): Eff[R, A] =
          fromEither(effect.toEither)

        override def guarantee[Env, E, A](f: Eff[R, A], cleanup: Eff[R, Unit]): Eff[R, A] =
          either.catchLeft(f)((_: Throwable) => cleanup.asInstanceOf[Eff[R, A]])(member)

        override def pure[A](a: A): Eff[R, A] = either.right(a)(member)
      }
    }

    // TODO: add errorInstances for org.atnos.eff.{option, validate, error, safe?}
  }
}
