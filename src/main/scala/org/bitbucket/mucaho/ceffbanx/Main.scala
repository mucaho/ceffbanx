package org.bitbucket.mucaho.ceffbanx

import cats.data.NonEmptySeq
import cats.effect.std.{Random, UUIDGen}
import cats.effect.{ExitCode, IO, IOApp, Sync}
import cats.{Applicative, Functor, MonadError}
import fs2.Stream
import io.github.timwspence.cats.stm.STM
import org.bitbucket.mucaho.ceffbanx.DefaultProgramRunner._
import org.bitbucket.mucaho.ceffbanx.domain.Common.{Balance, CustomerId, Standing}
import org.bitbucket.mucaho.ceffbanx.domain.Finance.{
  FinancialSuccessEvent,
  InsufficientCustomerBalance
}
import org.bitbucket.mucaho.ceffbanx.domain.ProgramTemplate.{
  constructLoanAndTransfer,
  constructTransfer
}
import tofu.logging.Logging
import tofu.streams.{Compile, Evals, ParFlatten}
import tofu.syntax.streams.all.{emits, eval, toParFlattenOps}

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    import tofu.fs2Instances._

    STM
      .runtime[IO]
      .flatMap(runWithSTM[Stream[IO, *], IO])
      .flatTap(outs => IO(println(outs)))
      .as(ExitCode.Success)
  }

  type MainResult = (
    (Balance, Map[CustomerId, Balance], Map[CustomerId, Standing]), // initialProgramState
    Seq[Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]], // events
    (Balance, Map[CustomerId, Balance], Map[CustomerId, Standing]), // finalProgramState
  )

  def runWithSTM[FS[_]: Evals[*[_], F]: ParFlatten: Compile[*[_], F], F[_]: Sync](
    stm: STM[F]
  ): F[MainResult] = {

    import cats.implicits._
    import tofu.streams._

    implicit val makeLogging: Logging.Make[F] = Logging.Make.plain[F]

    val out = for {
      mutableProgramState <- initializeMutableState(stm)
      random <- Random.scalaUtilRandom(Sync[F])

      initialProgramState <- readMutableState(stm)(mutableProgramState)

      events <- Compile[FS, F]
        .to[Seq, Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]] {
          createStreams(random)(stm)(mutableProgramState)
        }

      finalProgramState <- readMutableState(stm)(mutableProgramState)
    } yield (initialProgramState, events, finalProgramState)

    out
  }

  private def createStreams[FS[_]: Evals[*[_], F]: ParFlatten, F[_]: MonadError[
    *[_],
    Throwable
  ]: UUIDGen: Logging.Make]
  // TODO: move these repeated parameters into Ask context (random, stm, programState)
  (random: Random[F])(stm: STM[F])(
    programState: ProgramState[F, stm.type]
  ): FS[Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]] = {

    import cats.instances.seq._

    val iterations: Seq[Int] = 0 until 1000
    val stream = createStream(random)(stm)(programState)
    emits {
      iterations.map(_ => stream)
    }.parFlatten(Runtime.getRuntime.availableProcessors * 2)
  }

  private def createStream[FS[_]: Evals[*[_], F], F[_]: MonadError[
    *[_],
    Throwable
  ]: UUIDGen: Logging.Make](random: Random[F])(stm: STM[F])(
    programState: ProgramState[F, stm.type]
  ): FS[Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]] =
    eval(runIteration[F](random)(stm)(programState))

  private def runIteration[F[_]: MonadError[*[_], Throwable]: UUIDGen: Logging.Make](
    random: Random[F]
  )(stm: STM[F])(
    programState: ProgramState[F, stm.type]
  ): F[Either[Throwable, NonEmptySeq[FinancialSuccessEvent]]] = {

    import cats.implicits._
    import cats.mtl.Handle.handleForApplicativeError

    for {
      fromCustomerId <- generateAnyCustomerId(Functor[F], random)
      toCustomerId <- generateAnyCustomerId(Functor[F], random)
      amount <- generateAnyAmount(Functor[F], random)
      requestId <- UUIDGen[F].randomUUID

      out0 <- processProgram(stm)(programState)(
        constructTransfer(fromCustomerId, toCustomerId, amount, requestId)
      )
      out <- out0 match {
        case Left(_: InsufficientCustomerBalance) =>
          processProgram(stm)(programState)(
            constructLoanAndTransfer(fromCustomerId, toCustomerId, amount, requestId)
          )
        case _ @other => Applicative[F].pure(other)
      }
    } yield out
  }
}
