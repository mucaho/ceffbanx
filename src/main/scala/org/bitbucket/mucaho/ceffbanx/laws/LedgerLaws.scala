package org.bitbucket.mucaho.ceffbanx
package laws

import cats.Monad
import cats.laws._
import cats.syntax.all._
import eu.timepit.refined.refineV
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Ledger
import org.bitbucket.mucaho.ceffbanx.laws.IsEqConditionally.ToIsEqConditionallyOps

trait LedgerLaws[F[_]] {
  implicit def ledger: Ledger[F]
  implicit def monad: Monad[F]

  def getThenGetGetsOnce(customerId: CustomerId): IsEq[F[Standing]] =
    (ledger.getStandings(customerId) *> ledger.getStandings(customerId)) <-> ledger.getStandings(
      customerId
    )

  def adaptThenGetOnDifferentReturnsGetAdapt(
    customerId0: CustomerId,
    customerId1: CustomerId,
    amount: DeltaAmount
  ): IsEqConditionally[F[Standing]] =
    !customerId0.equals(customerId1) ==> (
      (ledger.adaptStandings(customerId0, amount) *> ledger.getStandings(customerId1)) <->
        (ledger.getStandings(customerId1) <* ledger.adaptStandings(customerId0, amount))
    )

  def adaptThenAdaptAdaptsSum(
    customerId: CustomerId,
    amount0: DeltaAmount,
    amount1: DeltaAmount
  ): IsEqConditionally[F[Boolean]] = {
    import eu.timepit.refined.auto._

    val amountTotal: Either[String, DeltaAmount] = refineV(amount0 + amount1)

    amountTotal.isRight ==> (
      (ledger.adaptStandings(customerId, amount0) *> ledger.adaptStandings(customerId, amount1)) <->
        ledger.adaptStandings(customerId, amountTotal.getOrElse(DELTA_ZERO))
    )
  }

  def adaptThenGetReturnsAdapted(customerId: CustomerId, amount: DeltaAmount): IsEq[F[Standing]] =
    (ledger.adaptStandings(customerId, amount) *> ledger.getStandings(customerId)) <->
      (ledger.getStandings(customerId) >>= { initialStanding =>
        ledger.adaptStandings(customerId, amount) >>= { adapted =>
          monad.pure(
            if (adapted)
              newStanding(initialStanding, amount).getOrElse(initialStanding)
            else initialStanding
          )
        }
      })

  def adaptThenInverseAdaptThenGetReturnsGet(
    customerId: CustomerId,
    amount: DeltaAmount
  ): IsEqConditionally[F[Standing]] = {
    import eu.timepit.refined.auto._

    val inverseAmount: Either[String, DeltaAmount] = refineV(-amount)

    inverseAmount.isRight ==> (
      (ledger.adaptStandings(customerId, amount) *> ledger.adaptStandings(
        customerId,
        inverseAmount.getOrElse(DELTA_ZERO)
      ) *> ledger.getStandings(customerId)) <->
        ledger.getStandings(customerId)
    )
  }
}

object LedgerLaws {
  def apply[F[_]](implicit instance: Ledger[F], monadInstance: Monad[F]): LedgerLaws[F] =
    new LedgerLaws[F] {
      override implicit def ledger: Ledger[F] = instance
      override implicit def monad: Monad[F] = monadInstance
    }
}
