package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import cats.kernel.laws.discipline.catsLawsIsEqToProp
import cats.{Eq, Monad}
import org.bitbucket.mucaho.ceffbanx.domain.Common.{CustomerId, DeltaAmount, Balance}
import org.bitbucket.mucaho.ceffbanx.domain.Account
import org.bitbucket.mucaho.ceffbanx.laws.AccountLaws
import org.scalacheck.Arbitrary
import org.scalacheck.Prop.forAll
import org.typelevel.discipline.Laws

trait AccountRuleSet[F[_]] extends Laws {
  implicit val account: Account[F]
  implicit val monad: Monad[F]

  def laws: AccountLaws[F] = AccountLaws[F]

  def ruleSet(implicit
    ArbCustomerId: Arbitrary[CustomerId],
    ArbDeltaAmount: Arbitrary[DeltaAmount],
    EqFBalance: Eq[F[Balance]],
    EqFBoolean: Eq[F[Boolean]]
  ): RuleSet = {

    new DefaultRuleSet(
      name = "account",
      parent = None,
      "get then get returns the same balance as one get" -> forAll(laws.getThenGetGetsOnce _),
      "adapt then adapt does the same as an adapt of both amounts" -> forAll(
        laws.adaptThenAdaptAdaptsSum _
      ),
      "adapt then get returns the adapted balance if successful, or the initial balance otherwise " -> forAll(
        laws.adaptThenGetReturnsAdapted _
      ),
      "adapt then adapt with inverse amount returns the initial balance" -> forAll(
        laws.adaptThenInverseAdaptThenGetReturnsGet _
      ),
      "adapt on another customer's balance does not influence this customer's balance" -> forAll(
        laws.adaptThenGetOnDifferentReturnsGetAdapt _
      )
    )
  }
}

object AccountRuleSet {
  def apply[F[_]](implicit instance: Account[F], monadInstance: Monad[F]): AccountRuleSet[F] = {
    new AccountRuleSet[F] with Laws {
      override implicit val account: Account[F] = instance
      override implicit val monad: Monad[F] = monadInstance
    }
  }
}
