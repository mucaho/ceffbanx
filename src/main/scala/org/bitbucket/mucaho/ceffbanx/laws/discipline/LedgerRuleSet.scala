package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import org.bitbucket.mucaho.ceffbanx.domain.Common.{CustomerId, DeltaAmount, Standing}
import org.bitbucket.mucaho.ceffbanx.domain.Ledger
import org.bitbucket.mucaho.ceffbanx.laws.LedgerLaws
import cats.{Eq, Monad}
import org.scalacheck.Arbitrary
import org.typelevel.discipline.Laws
import org.scalacheck.Prop.forAll
import cats.kernel.laws.discipline.catsLawsIsEqToProp

trait LedgerRuleSet[F[_]] extends Laws {
  implicit val ledger: Ledger[F]
  implicit val monad: Monad[F]

  def laws: LedgerLaws[F] = LedgerLaws[F]

  def ruleSet(implicit
    ArbCustomerId: Arbitrary[CustomerId],
    ArbDeltaAmount: Arbitrary[DeltaAmount],
    EqFStanding: Eq[F[Standing]],
    EqFBoolean: Eq[F[Boolean]]
  ): RuleSet = {

    new DefaultRuleSet(
      name = "ledger",
      parent = None,
      "get then get returns the same standing as one get" -> forAll(laws.getThenGetGetsOnce _),
      "adapt then adapt does the same as an adapt of both amounts" -> forAll(
        laws.adaptThenAdaptAdaptsSum _
      ),
      "adapt then get returns the adapted standing if successful, or the initial standing otherwise " -> forAll(
        laws.adaptThenGetReturnsAdapted _
      ),
      "adapt then adapt with inverse amount returns the initial standing" -> forAll(
        laws.adaptThenInverseAdaptThenGetReturnsGet _
      ),
      "adapt on another customer's standing does not influence this customer's standing" -> forAll(
        laws.adaptThenGetOnDifferentReturnsGetAdapt _
      )
    )
  }
}

object LedgerRuleSet {
  def apply[F[_]](implicit instance: Ledger[F], monadInstance: Monad[F]): LedgerRuleSet[F] = {
    new LedgerRuleSet[F] with Laws {
      override implicit val ledger: Ledger[F] = instance
      override implicit val monad: Monad[F] = monadInstance
    }
  }
}
