package org.bitbucket.mucaho.ceffbanx
package laws

import cats.data.NonEmptySeq
import cats.laws._
import eu.timepit.refined.refineV
import izumi.functional.bio.Monad2
import org.bitbucket.mucaho.ceffbanx.domain.Finance
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Finance.{
  FinancialErrorEvent,
  FinancialSuccessEvent,
  UpdatedBankBalance,
  UpdatedCustomerBalance,
  UpdatedCustomerStandings
}
import org.bitbucket.mucaho.ceffbanx.laws.IsEqConditionally.ToIsEqConditionallyOps

import java.util.UUID

trait FinanceLaws[F[+_, +_]] {
  implicit def finance: Finance[F]
  implicit def monad: Monad2[F]

  @annotation.nowarn @inline private def combined(
    a: NonEmptySeq[FinancialSuccessEvent],
    b: NonEmptySeq[FinancialSuccessEvent]
  ): Set[FinancialSuccessEvent] =
    a.toSeq.toSet ++ b.toSeq.toSet

  @inline private def unordered(
    evts: NonEmptySeq[FinancialSuccessEvent]
  ): Set[FinancialSuccessEvent] =
    evts.toSeq.toSet

  private def getBankAndCustomerState(
    customerId: CustomerId,
    requestId: UUID
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]] =
    finance.creditMoney(customerId, TRANSFER_ONE, requestId) *> finance
      .debitMoney(customerId, TRANSFER_ONE, requestId)

  private def getCustomerBalance(
    customerId: CustomerId,
    requestId: UUID
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]] =
    finance.transferMoney(customerId, customerId, TRANSFER_ONE, requestId)

  private def getCustomerStanding(
    customerId: CustomerId,
    requestId: UUID
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]] =
    getBankAndCustomerState(customerId, requestId)
      .map(_.collect { case evt: UpdatedCustomerStandings =>
        evt
      })
      .map(NonEmptySeq.fromSeqUnsafe)

  @SuppressWarnings(Array("org.wartremover.warts.IterableOps"))
  private def getBankBalance(
    requestId: UUID
  ): F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]] =
    getBankAndCustomerState(ALL_VALID_CUSTOMERS.head, requestId)
      .map(_.collect { case evt: UpdatedBankBalance =>
        evt
      })
      .map(NonEmptySeq.fromSeqUnsafe)

  private def updateCustomerBalance(
    events: NonEmptySeq[FinancialSuccessEvent]
  )(customerId: CustomerId)(amount: Int): NonEmptySeq[FinancialSuccessEvent] =
    events.map {
      case UpdatedCustomerBalance(`customerId`, initialBalance, requestId) =>
        UpdatedCustomerBalance(
          customerId,
          newBalance(initialBalance, amount).getOrElse(initialBalance),
          requestId
        )
      case _ @other => other
    }

  private def updateCustomerStanding(
    events: NonEmptySeq[FinancialSuccessEvent]
  )(customerId: CustomerId)(amount: Int): NonEmptySeq[FinancialSuccessEvent] =
    events.map {
      case UpdatedCustomerStandings(`customerId`, initialStanding, requestId) =>
        UpdatedCustomerStandings(
          customerId,
          newStanding(initialStanding, amount).getOrElse(initialStanding),
          requestId
        )
      case _ @other => other
    }

  private def updateBankBalance(
    events: NonEmptySeq[FinancialSuccessEvent]
  )(amount: Int): NonEmptySeq[FinancialSuccessEvent] =
    events.map {
      case UpdatedBankBalance(initialBalance, requestId) =>
        UpdatedBankBalance(newBalance(initialBalance, amount).getOrElse(initialBalance), requestId)
      case _ @other => other
    }

  def creditThenDebitReturnsDebitThenCredit(
    customerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEq[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (finance.creditMoney(customerId, amount, requestId) *> finance.debitMoney(
      customerId,
      amount,
      requestId
    )) <->
      (finance.debitMoney(customerId, amount, requestId) *> finance.creditMoney(
        customerId,
        amount,
        requestId
      ))

  def transferThenTransferReturnsSameAsInverse(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEq[F[FinancialErrorEvent, Set[FinancialSuccessEvent]]] =
    (finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) *> finance
      .transferMoney(toCustomerId, fromCustomerId, amount, requestId) map unordered) <->
      (finance.transferMoney(toCustomerId, fromCustomerId, amount, requestId) *> finance
        .transferMoney(fromCustomerId, toCustomerId, amount, requestId) map unordered)

  def creditThenCreditCreditsSum(
    customerId: CustomerId,
    amount0: TransferAmount,
    amount1: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] = {
    import eu.timepit.refined.auto._

    val amountTotal: Either[String, TransferAmount] = refineV(amount0 + amount1)

    amountTotal.isRight ==> (
      (finance.creditMoney(customerId, amount0, requestId) *> finance
        .creditMoney(customerId, amount1, requestId)) <->
        finance.creditMoney(customerId, amountTotal.getOrElse(TRANSFER_ONE), requestId)
    )
  }

  def debitThenDebitDebitsSum(
    customerId: CustomerId,
    amount0: TransferAmount,
    amount1: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] = {
    import eu.timepit.refined.auto._

    val amountTotal: Either[String, TransferAmount] = refineV(amount0 + amount1)

    amountTotal.isRight ==> (
      (finance.debitMoney(customerId, amount0, requestId) *> finance.debitMoney(
        customerId,
        amount1,
        requestId
      )) <->
        finance.debitMoney(customerId, amountTotal.getOrElse(TRANSFER_ONE), requestId)
    )
  }

  def transferThenTransferTransfersSum(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount0: TransferAmount,
    amount1: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] = {
    import eu.timepit.refined.auto._

    val amountTotal: Either[String, TransferAmount] = refineV(amount0 + amount1)

    (!fromCustomerId.equals(toCustomerId) && amountTotal.isRight) ==> (
      (finance.transferMoney(fromCustomerId, toCustomerId, amount0, requestId) *> finance
        .transferMoney(fromCustomerId, toCustomerId, amount1, requestId)) <->
        finance.transferMoney(
          fromCustomerId,
          toCustomerId,
          amountTotal.getOrElse(TRANSFER_ONE),
          requestId
        )
    )
  }

  def getThenGetGetsOnce0(
    customerId: CustomerId,
    requestId: UUID
  ): IsEq[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (getCustomerBalance(customerId, requestId) *> getCustomerBalance(
      customerId,
      requestId
    )) <-> getCustomerBalance(customerId, requestId)

  def getThenGetGetsOnce1(
    customerId: CustomerId,
    requestId: UUID
  ): IsEq[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (getBankAndCustomerState(customerId, requestId) *> getBankAndCustomerState(
      customerId,
      requestId
    )) <-> getBankAndCustomerState(customerId, requestId)

  def transferThenGetReturnsTransferred0(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !fromCustomerId.equals(toCustomerId) ==> (
      (finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) *> getCustomerBalance(
        toCustomerId,
        requestId
      )) <->
        (getCustomerBalance(toCustomerId, requestId) flatMap { initialEvts =>
          finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) flatMap { _ =>
            monad.pure(updateCustomerBalance(initialEvts)(toCustomerId)(+amount.value))
          }
        })
    )

  def transferThenGetReturnsTransferred1(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !fromCustomerId.equals(toCustomerId) ==> (
      (finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) *> getCustomerBalance(
        fromCustomerId,
        requestId
      )) <->
        (getCustomerBalance(fromCustomerId, requestId) flatMap { initialEvts =>
          finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) flatMap { _ =>
            monad.pure(updateCustomerBalance(initialEvts)(fromCustomerId)(-amount.value))
          }
        })
    )

  def transferThenGetReturnsUnchanged0(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !fromCustomerId.equals(toCustomerId) ==> (
      (finance.transferMoney(
        fromCustomerId,
        toCustomerId,
        amount,
        requestId
      ) *> getCustomerStanding(fromCustomerId, requestId)) <->
        (getCustomerStanding(fromCustomerId, requestId) flatMap { initialEvts =>
          finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) flatMap { _ =>
            monad.pure(initialEvts)
          }
        })
    )

  def transferThenGetReturnsUnchanged1(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !fromCustomerId.equals(toCustomerId) ==> (
      (finance.transferMoney(
        fromCustomerId,
        toCustomerId,
        amount,
        requestId
      ) *> getCustomerStanding(toCustomerId, requestId)) <->
        (getCustomerStanding(toCustomerId, requestId) flatMap { initialEvts =>
          finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) flatMap { _ =>
            monad.pure(initialEvts)
          }
        })
    )

  def transferThenGetReturnsUnchanged2(
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (!fromCustomerId.equals(toCustomerId) && amount != TRANSFER_MAX) ==> (
      (finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) *> getBankBalance(
        requestId
      )) <->
        (getBankBalance(requestId) flatMap { initialEvts =>
          finance.transferMoney(fromCustomerId, toCustomerId, amount, requestId) flatMap { _ =>
            monad.pure(initialEvts)
          }
        })
    )

  def creditThenGetReturnsCredited0(
    customerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEq[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (finance
      .creditMoney(customerId, amount, requestId) *> getCustomerBalance(customerId, requestId)) <->
      (getCustomerBalance(customerId, requestId) flatMap { initialEvts =>
        finance.creditMoney(customerId, amount, requestId) flatMap { _ =>
          monad.pure(updateCustomerBalance(initialEvts)(customerId)(+amount.value))
        }
      })

  def creditThenGetReturnsCredited1(
    customerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (amount != TRANSFER_MAX) ==> (
      (finance.creditMoney(customerId, amount, requestId) *> getBankBalance(requestId)) <->
        (getBankBalance(requestId) flatMap { initialEvts =>
          finance.creditMoney(customerId, amount, requestId) flatMap { _ =>
            monad.pure(updateBankBalance(initialEvts)(-amount.value))
          }
        })
    )

  def creditThenGetReturnsCredited2(
    customerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (amount != TRANSFER_MAX) ==> (
      (finance.creditMoney(customerId, amount, requestId) *> getCustomerStanding(
        customerId,
        requestId
      )) <->
        (getCustomerStanding(customerId, requestId) flatMap { initialEvts =>
          finance.creditMoney(customerId, amount, requestId) flatMap { _ =>
            monad.pure(updateCustomerStanding(initialEvts)(customerId)(-amount.value))
          }
        })
    )

  def debitThenGetReturnsDebited0(
    customerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEq[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (finance
      .debitMoney(customerId, amount, requestId) *> getCustomerBalance(customerId, requestId)) <->
      (getCustomerBalance(customerId, requestId) flatMap { initialEvts =>
        finance.debitMoney(customerId, amount, requestId) flatMap { _ =>
          monad.pure(updateCustomerBalance(initialEvts)(customerId)(-amount.value))
        }
      })

  def debitThenGetReturnsDebited1(
    customerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (amount != TRANSFER_MAX) ==> (
      (finance.debitMoney(customerId, amount, requestId) *> getBankBalance(requestId)) <->
        (getBankBalance(requestId) flatMap { initialEvts =>
          finance.debitMoney(customerId, amount, requestId) flatMap { _ =>
            monad.pure(updateBankBalance(initialEvts)(+amount.value))
          }
        })
    )

  def debitThenGetReturnsDebited2(
    customerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (amount != TRANSFER_MAX) ==> (
      (finance.debitMoney(customerId, amount, requestId) *> getCustomerStanding(
        customerId,
        requestId
      )) <->
        (getCustomerStanding(customerId, requestId) flatMap { initialEvts =>
          finance.debitMoney(customerId, amount, requestId) flatMap { _ =>
            monad.pure(updateCustomerStanding(initialEvts)(customerId)(+amount.value))
          }
        })
    )

  def transferThenGetOnDifferentReturnsGetTransfer0(
    otherCustomerId: CustomerId,
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (!otherCustomerId.equals(fromCustomerId) && !otherCustomerId.equals(
      toCustomerId
    ) && !fromCustomerId.equals(toCustomerId)) ==>
      ((finance.transferMoney(
        fromCustomerId,
        toCustomerId,
        amount,
        requestId
      ) *> getCustomerBalance(otherCustomerId, requestId)) <->
        (getCustomerBalance(otherCustomerId, requestId) <* finance
          .transferMoney(fromCustomerId, toCustomerId, amount, requestId)))

  def transferThenGetOnDifferentReturnsGetTransfer1(
    otherCustomerId: CustomerId,
    fromCustomerId: CustomerId,
    toCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    (!otherCustomerId.equals(fromCustomerId) && !otherCustomerId.equals(
      toCustomerId
    ) && !fromCustomerId.equals(toCustomerId)) ==>
      ((finance.transferMoney(
        fromCustomerId,
        toCustomerId,
        amount,
        requestId
      ) *> getCustomerStanding(otherCustomerId, requestId)) <->
        (getCustomerStanding(otherCustomerId, requestId) <* finance
          .transferMoney(fromCustomerId, toCustomerId, amount, requestId)))

  def creditThenGetOnDifferentReturnsGetCredit0(
    customerId: CustomerId,
    otherCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !customerId.equals(otherCustomerId) ==>
      ((finance.creditMoney(customerId, amount, requestId) *> getCustomerBalance(
        otherCustomerId,
        requestId
      )) <->
        (getCustomerBalance(otherCustomerId, requestId) <* finance.creditMoney(
          customerId,
          amount,
          requestId
        )))

  def creditThenGetOnDifferentReturnsGetCredit1(
    customerId: CustomerId,
    otherCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !customerId.equals(otherCustomerId) ==>
      ((finance.creditMoney(customerId, amount, requestId) *> getCustomerStanding(
        otherCustomerId,
        requestId
      )) <->
        (getCustomerStanding(otherCustomerId, requestId) <* finance.creditMoney(
          customerId,
          amount,
          requestId
        )))

  def debitThenGetOnDifferentReturnsGetDebit0(
    customerId: CustomerId,
    otherCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !customerId.equals(otherCustomerId) ==>
      ((finance.debitMoney(customerId, amount, requestId) *> getCustomerBalance(
        otherCustomerId,
        requestId
      )) <->
        (getCustomerBalance(otherCustomerId, requestId) <* finance.debitMoney(
          customerId,
          amount,
          requestId
        )))

  def debitThenGetOnDifferentReturnsGetDebit1(
    customerId: CustomerId,
    otherCustomerId: CustomerId,
    amount: TransferAmount,
    requestId: UUID
  ): IsEqConditionally[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]] =
    !customerId.equals(otherCustomerId) ==>
      ((finance.debitMoney(customerId, amount, requestId) *> getCustomerStanding(
        otherCustomerId,
        requestId
      )) <->
        (getCustomerStanding(otherCustomerId, requestId) <* finance.debitMoney(
          customerId,
          amount,
          requestId
        )))
}

object FinanceLaws {
  def apply[F[+_, +_]](implicit instance: Finance[F], monadInstance: Monad2[F]): FinanceLaws[F] =
    new FinanceLaws[F] {
      override implicit def finance: Finance[F] = instance
      override implicit def monad: Monad2[F] = monadInstance
    }
}
