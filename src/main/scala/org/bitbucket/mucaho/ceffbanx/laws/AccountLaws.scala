package org.bitbucket.mucaho.ceffbanx
package laws

import cats.Monad
import cats.laws._
import cats.syntax.all._
import eu.timepit.refined.refineV
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.domain.Account
import org.bitbucket.mucaho.ceffbanx.laws.IsEqConditionally.ToIsEqConditionallyOps

trait AccountLaws[F[_]] {
  implicit def account: Account[F]
  implicit def monad: Monad[F]

  def getThenGetGetsOnce(customerId: CustomerId): IsEq[F[Balance]] =
    (account.getBalance(customerId) *> account.getBalance(customerId)) <-> account.getBalance(
      customerId
    )

  def adaptThenGetOnDifferentReturnsGetAdapt(
    customerId0: CustomerId,
    customerId1: CustomerId,
    amount: DeltaAmount
  ): IsEqConditionally[F[Balance]] =
    !customerId0.equals(customerId1) ==>
      ((account.adaptBalanceBy(customerId0, amount) *> account.getBalance(customerId1)) <->
        (account.getBalance(customerId1) <* account.adaptBalanceBy(customerId0, amount)))

  def adaptThenAdaptAdaptsSum(
    customerId: CustomerId,
    amount0: DeltaAmount,
    amount1: DeltaAmount
  ): IsEqConditionally[F[Boolean]] = {
    import eu.timepit.refined.auto._

    val amountTotal: Either[String, DeltaAmount] = refineV(amount0 + amount1)

    (amount0 >= 0 && amount0 + amount1 >= 0 && amountTotal.isRight) ==> (
      (account
        .adaptBalanceBy(customerId, amount0) *> account.adaptBalanceBy(customerId, amount1)) <->
        account.adaptBalanceBy(customerId, amountTotal.getOrElse(DELTA_ZERO))
    )
  }

  def adaptThenGetReturnsAdapted(customerId: CustomerId, amount: DeltaAmount): IsEq[F[Balance]] =
    (account.adaptBalanceBy(customerId, amount) *> account.getBalance(customerId)) <->
      (account.getBalance(customerId) >>= { initialBalance =>
        account.adaptBalanceBy(customerId, amount) >>= { adapted =>
          monad.pure(
            if (adapted)
              newBalance(initialBalance, amount).getOrElse(initialBalance)
            else initialBalance
          )
        }
      })

  def adaptThenInverseAdaptThenGetReturnsGet(
    customerId: CustomerId,
    amount: DeltaAmount
  ): IsEqConditionally[F[Balance]] = {
    import eu.timepit.refined.auto._

    val inverseAmount: Either[String, DeltaAmount] = refineV(-amount)

    (amount >= 0 && inverseAmount.isRight) ==> (
      (account.adaptBalanceBy(customerId, amount) *> account.adaptBalanceBy(
        customerId,
        inverseAmount.getOrElse(DELTA_ZERO)
      ) *> account.getBalance(customerId)) <->
        account.getBalance(customerId)
    )
  }
}

object AccountLaws {
  def apply[F[_]](implicit instance: Account[F], monadInstance: Monad[F]): AccountLaws[F] =
    new AccountLaws[F] {
      override implicit def account: Account[F] = instance
      override implicit def monad: Monad[F] = monadInstance
    }
}
