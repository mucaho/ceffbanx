package org.bitbucket.mucaho.ceffbanx
package laws

import cats.Monad
import cats.laws._
import cats.syntax.all._
import eu.timepit.refined.refineV
import org.bitbucket.mucaho.ceffbanx.domain.Bank
import org.bitbucket.mucaho.ceffbanx.domain.Common._
import org.bitbucket.mucaho.ceffbanx.laws.IsEqConditionally.ToIsEqConditionallyOps

trait BankLaws[F[_]] {
  implicit def bank: Bank[F]
  implicit def monad: Monad[F]

  def getThenGetGetsOnce: IsEq[F[Balance]] =
    (bank.getBalance *> bank.getBalance) <-> bank.getBalance

  def adaptThenAdaptAdaptsSum(
    amount0: DeltaAmount,
    amount1: DeltaAmount
  ): IsEqConditionally[F[Boolean]] = {
    import eu.timepit.refined.auto._

    val amountTotal: Either[String, DeltaAmount] = refineV(amount0 + amount1)

    (amount0 >= 0 && amount0 + amount1 >= 0 && amountTotal.isRight) ==> (
      (bank.adaptBalanceBy(amount0) *> bank.adaptBalanceBy(amount1)) <->
        bank.adaptBalanceBy(amountTotal.getOrElse(DELTA_ZERO))
    )
  }

  def adaptThenGetReturnsAdapted(amount: DeltaAmount): IsEq[F[Balance]] =
    (bank.adaptBalanceBy(amount) *> bank.getBalance) <->
      (bank.getBalance >>= { initialBalance =>
        bank.adaptBalanceBy(amount) >>= { adapted =>
          monad.pure(
            if (adapted)
              newBalance(initialBalance, amount).getOrElse(initialBalance)
            else initialBalance
          )
        }
      })

  def adaptThenInverseAdaptThenGetReturnsGet(amount: DeltaAmount): IsEqConditionally[F[Balance]] = {
    import eu.timepit.refined.auto._

    val inverseAmount: Either[String, DeltaAmount] = refineV(-amount)

    (amount >= 0 && inverseAmount.isRight) ==> (
      (bank.adaptBalanceBy(amount) *> bank.adaptBalanceBy(
        inverseAmount.getOrElse(DELTA_ZERO)
      ) *> bank.getBalance) <->
        bank.getBalance
    )
  }
}

object BankLaws {
  def apply[F[_]](implicit instance: Bank[F], monadInstance: Monad[F]): BankLaws[F] =
    new BankLaws[F] {
      override implicit def bank: Bank[F] = instance
      override implicit def monad: Monad[F] = monadInstance
    }
}
