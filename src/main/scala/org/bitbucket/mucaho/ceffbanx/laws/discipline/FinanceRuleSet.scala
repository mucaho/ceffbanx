package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import cats.data.NonEmptySeq
import cats.kernel.laws.discipline.catsLawsIsEqToProp
import cats.Eq
import izumi.functional.bio.Monad2
import org.bitbucket.mucaho.ceffbanx.domain.Finance
import org.bitbucket.mucaho.ceffbanx.domain.Common.{CustomerId, TransferAmount}
import org.bitbucket.mucaho.ceffbanx.domain.Finance.{FinancialErrorEvent, FinancialSuccessEvent}
import org.bitbucket.mucaho.ceffbanx.laws.FinanceLaws
import org.scalacheck.Arbitrary
import org.scalacheck.Prop.forAll
import org.typelevel.discipline.Laws

import java.util.UUID

trait FinanceRuleSet[F[+_, +_]] extends Laws {
  implicit val finance: Finance[F]
  implicit val monad: Monad2[F]

  def laws: FinanceLaws[F] = FinanceLaws[F]

  def ruleSet(implicit
    ArbCustomerId: Arbitrary[CustomerId],
    ArbTransferAmount: Arbitrary[TransferAmount],
    ArbUUID: Arbitrary[UUID],
    EqFErrorSuccessEvt: Eq[F[FinancialErrorEvent, NonEmptySeq[FinancialSuccessEvent]]],
    EqFErrorSuccessEvt2: Eq[F[FinancialErrorEvent, Set[FinancialSuccessEvent]]]
  ): RuleSet = {

    new DefaultRuleSet(
      name = "finance",
      parent = None,
      "credit then debit returns the result as vice versa" -> forAll(
        laws.creditThenDebitReturnsDebitThenCredit _
      ),
      "transfer then transfer back returns the same result as vice versa" -> forAll(
        laws.transferThenTransferReturnsSameAsInverse _
      ),
      "credit then credit does the same as a credit of both amounts" -> forAll(
        laws.creditThenCreditCreditsSum _
      ),
      "debit then debit does the same as a debit of both amounts" -> forAll(
        laws.debitThenDebitDebitsSum _
      ),
      "transfer then transfer does the same as a transfer of both amounts" -> forAll(
        laws.transferThenTransferTransfersSum _
      ),
      "get then get returns the same balance as one get for customer balance" -> forAll(
        laws.getThenGetGetsOnce0 _
      ),
      "get then get returns the same balance as one get for customer and bank state" -> forAll(
        laws.getThenGetGetsOnce1 _
      ),
      "transfer then get returns the added balance if successful, or the initial receiving customer's balance otherwise " -> forAll(
        laws.transferThenGetReturnsTransferred0 _
      ),
      "transfer then get returns the subtracted balance if successful, or the initial sending customer's balance otherwise " -> forAll(
        laws.transferThenGetReturnsTransferred1 _
      ),
      "transfer does not change sending customer standings" -> forAll(
        laws.transferThenGetReturnsUnchanged0 _
      ),
      "transfer does not change receiving customer standings" -> forAll(
        laws.transferThenGetReturnsUnchanged1 _
      ),
      "transfer does not change bank balance" -> forAll(laws.transferThenGetReturnsUnchanged2 _),
      "credit then get returns the added balance if successful, or the initial customer balance otherwise " -> forAll(
        laws.creditThenGetReturnsCredited0 _
      ),
      "credit then get returns the subtracted balance if successful, or the initial bank balance otherwise " -> forAll(
        laws.creditThenGetReturnsCredited1 _
      ),
      "credit then get returns the subtracted standing if successful, or the initial customer standing otherwise " -> forAll(
        laws.creditThenGetReturnsCredited2 _
      ),
      "debit then get returns the added balance if successful, or the initial customer balance otherwise " -> forAll(
        laws.debitThenGetReturnsDebited0 _
      ),
      "debit then get returns the subtracted balance if successful, or the initial bank balance otherwise " -> forAll(
        laws.debitThenGetReturnsDebited1 _
      ),
      "debit then get returns the subtracted standing if successful, or the initial customer standing otherwise " -> forAll(
        laws.debitThenGetReturnsDebited2 _
      ),
      "transfer between other customers does not influence this customer's balance" -> forAll(
        laws.transferThenGetOnDifferentReturnsGetTransfer0 _
      ),
      "transfer between other customers does not influence this customer's standing" -> forAll(
        laws.transferThenGetOnDifferentReturnsGetTransfer1 _
      ),
      "credit on other customer does not influence this customer's balance" -> forAll(
        laws.creditThenGetOnDifferentReturnsGetCredit0 _
      ),
      "credit on other customer does not influence this customer's standing" -> forAll(
        laws.creditThenGetOnDifferentReturnsGetCredit1 _
      ),
      "debit on other customer does not influence this customer's balance" -> forAll(
        laws.debitThenGetOnDifferentReturnsGetDebit0 _
      ),
      "debit on other customer does not influence this customer's standing" -> forAll(
        laws.debitThenGetOnDifferentReturnsGetDebit1 _
      )
    )
  }
}

object FinanceRuleSet {
  def apply[F[+_, +_]](implicit
    instance: Finance[F],
    monadInstance: Monad2[F]
  ): FinanceRuleSet[F] = {
    new FinanceRuleSet[F] with Laws {
      override implicit val finance: Finance[F] = instance
      override implicit val monad: Monad2[F] = monadInstance
    }
  }
}
