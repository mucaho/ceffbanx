package org.bitbucket.mucaho.ceffbanx
package laws

import cats.kernel.Eq
import cats.laws.IsEq
import org.scalacheck.Prop
import org.scalacheck.util.Pretty

// import language.implicitConversions

final class IsEqConditionally[A](val condition: Boolean, _isEq: => IsEq[A]) {
  lazy val isEq: IsEq[A] = _isEq
}

object IsEqConditionally {
  def apply[A](condition: Boolean, isEq: => IsEq[A]) =
    new IsEqConditionally(condition, isEq)

  implicit class ToIsEqConditionallyOps(condition: Boolean) {
    def ==>[A](isEq: => IsEq[A]): IsEqConditionally[A] =
      IsEqConditionally(condition, isEq)
  }

  implicit def isEqConditionallyToProp[A](
    isEqConditionally: IsEqConditionally[A]
  )(implicit ev: Eq[A], pp: A => Pretty): Prop =
    Prop(isEqConditionally.condition) ==> cats.kernel.laws.discipline
      .catsLawsIsEqToProp(isEqConditionally.isEq)
}
