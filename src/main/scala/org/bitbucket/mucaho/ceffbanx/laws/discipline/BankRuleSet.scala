package org.bitbucket.mucaho.ceffbanx
package laws.discipline

import cats.kernel.laws.discipline.catsLawsIsEqToProp
import cats.{Eq, Monad}
import org.bitbucket.mucaho.ceffbanx.domain.Bank
import org.bitbucket.mucaho.ceffbanx.domain.Common.{Balance, DeltaAmount}
import org.bitbucket.mucaho.ceffbanx.laws.BankLaws
import org.scalacheck.Arbitrary
import org.scalacheck.Prop.forAll
import org.typelevel.discipline.Laws

trait BankRuleSet[F[_]] extends Laws {
  implicit val bank: Bank[F]
  implicit val monad: Monad[F]

  def laws: BankLaws[F] = BankLaws[F]

  def ruleSet(implicit
    ArbDeltaAmount: Arbitrary[DeltaAmount],
    EqFBalance: Eq[F[Balance]],
    EqFBoolean: Eq[F[Boolean]]
  ): RuleSet = {

    new DefaultRuleSet(
      name = "bank",
      parent = None,
      "get then get returns the same balance as one get" -> laws.getThenGetGetsOnce,
      "adapt then adapt does the same as an adapt of both amounts" -> forAll(
        laws.adaptThenAdaptAdaptsSum _
      ),
      "adapt then get returns the adapted balance if successful, or the initial balance otherwise " -> forAll(
        laws.adaptThenGetReturnsAdapted _
      ),
      "adapt then adapt with inverse amount returns the initial balance" -> forAll(
        laws.adaptThenInverseAdaptThenGetReturnsGet _
      )
    )
  }
}

object BankRuleSet {
  def apply[F[_]](implicit instance: Bank[F], monadInstance: Monad[F]): BankRuleSet[F] = {
    new BankRuleSet[F] with Laws {
      override implicit val bank: Bank[F] = instance
      override implicit val monad: Monad[F] = monadInstance
    }
  }
}
