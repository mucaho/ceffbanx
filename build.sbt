name := "CEffBanX"
organization := "org.bitbucket.mucaho"
version := "0.1.0"

scalaVersion := "2.13.10"

// idePackagePrefix := Some("org.bitbucket.mucaho.ceffbanx")

scalafmtOnCompile := true
scalafixOnCompile := true
scalafixCaching := false
semanticdbEnabled := true
semanticdbOptions += "-P:semanticdb:synthetics:on"
semanticdbVersion := scalafixSemanticdb.revision
scalafixScalaBinaryVersion := CrossVersion.binaryScalaVersion(scalaVersion.value)
// TODO: updgrade SBT, properly structure this build file, extract dependencies to Dependencies.scala,
//  and figure out why additional scalafix rules from dependencies are not run
scalafixDependencies += "com.github.vovapolu" %% "scaluzzi" % "0.1.23"
scalafixDependencies += "net.pixiv" %% "scalafix-pixiv-rule" % "4.2.0"
scalafixDependencies += "io.github.ghostbuster91.scalafix-unified" %% "unified" % "0.0.8"
scalafixDependencies += "org.typelevel" %% "typelevel-scalafix" % "0.1.5"
scalafixDependencies += "com.github.xuwei-k" %% "scalafix-rules" % "0.2.18"
scalafixDependencies += "com.github.liancheng" %% "organize-imports" % "0.6.0"
wartremoverErrors ++= Warts.unsafe
wartremoverErrors --= Seq(Wart.Any, Wart.Nothing, Wart.Product, Wart.Serializable)
// wartremoverWarnings ++= Warts.allBut(Wart.Any, Wart.Nothing, Wart.Product, Wart.Serializable)
// TODO: add scapegoat
// scapegoatVersion in ThisBuild := "2.1.1"

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)
addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
// TODO: use compiler plugin for conveniently summoning typeclass instances
// addCompilerPlugin("org.augustjune" %% "context-applied" % "0.1.4")

// libraryDependencies += "org.typelevel" %% "mouse" % "1.2.1"
libraryDependencies += "org.typelevel" %% "cats-core" % "2.8.0"
libraryDependencies += "org.typelevel" %% "cats-testkit" % "2.8.0"
// libraryDependencies += "org.typelevel" %% "cats-free" % "2.8.0"
libraryDependencies += "org.typelevel" %% "cats-mtl" % "1.3.0"
libraryDependencies += "org.typelevel" %% "cats-effect" % "3.3.14"
// libraryDependencies += "com.olegpy" %% "meow-mtl-core" % "0.5.0"
// libraryDependencies += "com.olegpy" %% "meow-mtl-effects" % "0.5.0" exclude("org.typelevel", s"cats-effect_${scalaBinaryVersion.value}")
libraryDependencies += "org.typelevel" %% "kittens" % "3.0.0"
// libraryDependencies += "io.monix" %% "monix" % "3.4.0"
// libraryDependencies += "io.monix" %% "monix-bio" % "1.2.0"
libraryDependencies += "io.7mind.izumi" %% "fundamentals-bio" % "1.1.0-M19"
libraryDependencies += "tf.tofu" %% "tofu-kernel" % "0.11.1"
libraryDependencies += "tf.tofu" %% "tofu-kernel-cats-mtl" % "0.11.1"
libraryDependencies += "tf.tofu" %% "tofu-core-ce3" % "0.11.1"
libraryDependencies += "tf.tofu" %% "tofu-core-higher-kind" % "0.11.1"
libraryDependencies += "tf.tofu" %% "tofu-streams" % "0.11.1"
libraryDependencies += "tf.tofu" %% "tofu-fs2-ce3-interop" % "0.11.1"
libraryDependencies += "tf.tofu" %% "tofu-logging" % "0.11.1" exclude("org.typelevel", s"log4cats-core_${scalaBinaryVersion.value}")
libraryDependencies += "org.atnos" %% "eff" % "6.0.1"
libraryDependencies += "org.atnos" %% "eff-cats-effect" % "6.0.1"
// libraryDependencies += "io.parapet" %% "core" % "0.0.1-RC6"
// libraryDependencies += "io.parapet" %% "interop-cats" % "0.0.1-RC6"
libraryDependencies += "co.fs2" %% "fs2-core" % "3.2.10"
// libraryDependencies += "co.fs2" %% "fs2-reactive-streams" % "3.2.10"
libraryDependencies += "io.github.timwspence" %% "cats-stm" % "0.13.1"
libraryDependencies += "io.monix" %% "newtypes-core" % "0.2.3"
// libraryDependencies += "massimosiani" %% "monix-newtypes-cats" % "0.0.2"
// dependsOn(ProjectRef(uri("https://github.com/massimosiani/monix-newtypes-cats.git"), "monix-newtypes-cats"))
libraryDependencies += "eu.timepit" %% "refined" % "0.10.1"
libraryDependencies += "eu.timepit" %% "refined-cats" % "0.10.1"
libraryDependencies += "eu.timepit" %% "refined-scalacheck" % "0.10.1"
// libraryDependencies += "eu.timepit" %% "refined-shapeless" % "0.10.1"
libraryDependencies += "org.typelevel" %% "log4cats-core" % "2.5.0"
libraryDependencies += "org.typelevel" %% "log4cats-slf4j" % "2.5.0"
libraryDependencies += "org.tpolecat" %% "natchez-core" % "0.3.1"
libraryDependencies += "org.tpolecat" %% "natchez-log" % "0.3.1"
// libraryDependencies += "io.janstenpickle" %% "trace4cats-core" % "0.14.0"
// libraryDependencies += "io.janstenpickle" %% "trace4cats-fs2" % "0.14.0"
// libraryDependencies += "io.janstenpickle" %% "trace4cats-natchez" % "0.14.0"

// resolvers += Opts.resolver.sonatypeSnapshots
// libraryDependencies += "de.b-studios" % "effekt_2.12" % "0.4-SNAPSHOT" // no 2.13 release!
// libraryDependencies += "de.b-studios" % "effekt-effects_2.12" % "0.4-SNAPSHOT" exclude("org.typelevel", "cats-core_2.12") exclude("org.typelevel", "cats-kernel_2.12")
// dependsOn(RootProject(uri("https://github.com/b-studios/scala-effekt.git#403a568940b011dd46ac076b0ecf4b723fe99be1")))
dependsOn(ProjectRef(uri("https://github.com/mucaho/scala-effekt.git"), "effektJVM"))
dependsOn(ProjectRef(uri("https://github.com/mucaho/scala-effekt.git"), "effectsJVM"))

libraryDependencies += "com.disneystreaming" %% "weaver-cats" % "0.8.2" % Test
libraryDependencies += "com.disneystreaming" %% "weaver-scalacheck" % "0.8.2" % Test
libraryDependencies += "com.disneystreaming" %% "weaver-discipline" % "0.8.2" % Test
libraryDependencies += "io.github.dimitarg"  %% "weaver-test-extra" % "0.5.5" % Test
testFrameworks += new TestFramework("weaver.framework.CatsEffect")
