// addSbtPlugin("org.jetbrains" % "sbt-ide-settings" % "1.1.0")
addSbtPlugin("io.github.davidgregory084" % "sbt-tpolecat" % "0.4.2")
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.10.4")
addSbtPlugin("org.wartremover" % "sbt-wartremover" % "3.1.0")
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.6")
// TODO: add scapegoat
// addSbtPlugin("com.sksamuel.scapegoat" % "sbt-scapegoat" % "2.1.1")
